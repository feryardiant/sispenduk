<?php defined('ABSPATH') or die ('Not allowed!');

if (!$userMod->loggedin()) redirect('../?user=login');

if (!($sub = App::uriSegment(2))) {
    $sub = 'penduduk';
} elseif (!in_array($sub, array('penduduk', 'keluarga'))) {
    App::show404();
}

template('header');

$toolbars = array();
$userLevel = $userMod->current('level');

switch ($page) {
    case 'form':
        $file = 'form/'.$sub;
        $toolbars['warga/'.$sub] = 'Kembali';
        $id = false;
        $fields = array(
            'penduduk' => array(
                'nik', 'no_kk', 'nama',
                'jns_kel', 'tmpt_lahir', 'tgl_lahir', 'gol_darah', 'agama',
                'status_kawin', 'status_kel', 'pendidikan', 'pekerjaan', 'nama_ibu', 'nama_ayah',
                'alamat', 'rt', 'rw', 'pos', 'kel', 'kec', 'kota', 'prop',
            ),
            'keluarga' => array(
                'no_kk', 'nik_kepala',
                'alamat', 'rt', 'rw', 'pos', 'kel', 'kec', 'kota', 'prov'
            ),
        );

        if (post('submit')) {
            if ($wargaMod->save($sub, $fields[$sub])) {
                App::alert('Data '.$sub.' berhasil disimpan', 'success');
                redirect('?p=data', 3);
            } else {
                App::alert('Terjadi kesalahan penyimpanan data '.$sub.'', 'error');
            }
        } else {
            $avail = !in_array($userLevel, array(1, 4)) ? 'disabled' : '';
            if ($id = get('id')) {
                $key = $sub == 'penduduk' ? 'nik' : 'no_kk';
                $data = $wargaMod->get($sub, $id, $key);
            }
        }

        break;

    case 'hapus':
        $file = 'data';
        if ($wargaMod->delete($sub)) {
            App::alert('Data '.$sub.' berhasil dihapus', 'success');
            redirect('?p=data');
        } else {
            App::alert('Terjadi kesalahan dalam penghapusan data '.$sub, 'error');
        }
        break;

    case 'data':
    default:
        $file = $page;
        $toolbars['warga/'.$sub.'?p=pdf'] = 'Export PDF';

        if (in_array($userLevel, array(1, 4))) {
            $toolbars['warga/'.$sub.'?p=form'] = 'Baru';
        }

        // $toolbars[1] = array(
        //     'warga/penduduk' => 'Data Penduduk',
        //     'warga/keluarga' => 'Data Keluarga'
        // );
        break;
}

$judul = ucfirst($page).' '.ucfirst($sub); ?>
<header id="content-header" class="clearfix">
    <h3 id="page-title"><?php echo $judul ?></h3>
    <?php echo Menu::toolbar($toolbars) ?>
</header>
<div id="content-main" class="clearfix">
    <?php include $file.'.php' ?>
</div>
<?php template('footer') ?>
