<?php defined('ABSPATH') or die ('Not allowed!');

$del_message = 'Apakah anda yakin ingin menghapus data tersebut.';
$columns     = array(
    'no_kk' => array('label' => 'No. KK', 'width' => 15)
);

if ($sub == 'keluarga') {
    $columns['nama_kk'] = array('width' => 15, 'label' => 'Nama Kepala Keluarga');
    $columns['alamat']  = array('width' => 40, 'label' => 'Alamat');
} else {
    $columns['nik']     = array('width' => 15, 'label' => 'NIK');
    $columns['nama']    = array('width' => 40, 'label' => 'Nama Lengkap');
    $columns['jns_kel'] = array('width' => 10, 'label' => 'Jenis Kelamin', '_call' => function ($_jns_kel) {
        return Warga::$prop['jns_kel'][$_jns_kel];
    }, 'extra' => 'class="acenter"');
}

$table = new Table($wargaMod->fetch($sub), $columns);
// $table->rowNum = true;
$table->setButton('nik', 'warga/'.$sub.'?p=form&id=', 'Lihat', array('class' => 'btn btn-edit'));
$table->setButton('nik', 'warga/'.$sub.'?p=hapus&id=', 'Hapus', array('class' => 'btn btn-hapus', 'data-confirm-text' => $del_message));

echo $table->generate();
