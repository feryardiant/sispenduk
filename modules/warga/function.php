<?php defined('ABSPATH') or die ('Not allowed!');

class Warga extends Module
{
    static $tables = array(
        'penduduk' => 'tbl_penduduk',
        'keluarga' => 'tbl_keluarga',
    );

    static $prop = array(
        'jns_kel'      => array('Perempuan', 'Laki-laki'),
        'agama'        => array('Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya'),
        'gol_darah'    => array('A', 'B', 'AB', 'O'),
        'status_kawin' => array('Belum Menikah', 'Sudah Menikah'),
        'kwn'          => array('Warga Negara Indonesia', 'Warga Negara Asing'),
        'status_kel'   => array('Kepala Keluarga', 'Suami', 'Istri', 'Anak', 'Menantu', 'Cucu', 'Orang Tua', 'Mertua', 'Famili Lain', 'Lainnya'),
        'alamat'       => 'Alamat Lengkap',
        'rt'           => '002',
        'rw'           => '001',
        'pos'          => '54321',
        'kel'          => 'Nama Kelurahan',
        'kec'          => 'Nama Kecamatan',
        'kota'         => 'Kab. Pekalongan',
        'prop'         => 'Jawa Tengah',
    );

    function __construct() {
        parent::__construct();
        $this->menu->add($this, array(
            'caps' => array(1, 2, 3, 4),
            'title' => 'Kependudukan',
        ));

        if (App::uriSegment(1) == 'warga' && get('p') == 'pdf') {
            $this->getPdf();
            exit();
        }
    }

    public function api() {
        $result = array('empty');
        $table = get('table');

        if (($nik = get('nik')) && ($query = self::find($table, $nik, 'nik'))) {
            if ($query->count() > 0) {
                $result = array();
                foreach ($query->result() as $row) {
                    $result[] = $row;
                }
            }
        }

        echo json_encode($result);
    }

    public function find($table, $val = false, $key = '') {
        if ($val !== false) {
            $key || $key = 'nik';
        }

        return static::$db->query("SELECT * FROM %s WHERE %s LIKE '%s'", array(self::$tables[$table], $key, '%'.$val.'%'));
    }

    public function fetch($table, $val = false, $key = '') {
        $where = array();

        if ($val !== false && $key != '') {
            $where = array($key => $val);
        }

        return static::$db->select(self::$tables[$table], '', $where);
    }

    public function fetchOne($table, $val = false, $key = '') {
        if ($data = $this->get($table, $val, $key)) {
            foreach ($data as $field => $value) {
                if (in_array($field, array('jns_kel', 'agama', 'gol_darah', 'status_kawin', 'kwn', 'status_kel'))) {
                    $value = self::$prop[$field][$value];
                }
                if ($field == 'tgl_lahir') {
                    $value = formatTanggal($value);
                }
                $data->$field = $value;
            }
            return $data;
        }
    }

    public function get($table, $val = false, $key = '') {
        if ($query = self::fetch($table, $val, $key)) {
            return $query->fetchOne();
        }

        return false;
    }

    public function save($table, $fields) {
        $data = array();

        foreach ($fields as $field) {
            $data[$field] = post($field);

            if (in_array($field, array('rt', 'rw'))) {
                $data[$field] = substr('000'.$data[$field], -3);
            }
        }

        if (isset($data['tgl_lahir']) && !empty($data['tgl_lahir'])) {
            $data['tgl_lahir'] = date('Y-m-d', strtotime($data['tgl_lahir']));
        }

        $key = $table == 'penduduk' ? 'nik' : 'no_kk';
        $term = ($id = get('id')) ? array($key => $id) : array();

        return static::$db->save(self::$tables[$table], $data, $term);
    }

    public function delete($table) {
        $key = $table == 'penduduk' ? 'nik' : 'no_kk';
        $term = ($id = get('id')) ? array($key => $id) : array();

        return static::$db->delete(self::$tables[$table], $term);
    }

    public function getPdf() {
        require dirname(__FILE__).DS.'pdf'.EXT;

        try {
            $pdf = new WargaPdf();
            echo $pdf->lapor(self::fetch('penduduk'))->toFile();
            // var_dump(self::fetch('penduduk')->result());
        } catch (Exception $e) {
            App::alert($e);
        }
    }
}
