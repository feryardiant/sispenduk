--
-- MySQL 5.5.40
-- Tue, 18 Nov 2014 06:56:29 +0000
--

DROP TABLE IF EXISTS `tbl_penduduk`;
CREATE TABLE `tbl_penduduk` (
   `nik` char(16) not null,
   `no_kk` char(16) not null,
   `nama` varchar(100) not null,
   `jns_kel` tinyint(1) not null,
   `tmpt_lahir` varchar(50) not null,
   `tgl_lahir` date not null,
   `gol_darah` tinyint(1) not null,
   `agama` tinyint(1) not null,
   `status_kawin` tinyint(1) not null,
   `status_kel` tinyint(1) not null,
   `pendidikan` varchar(50) not null,
   `pekerjaan` varchar(50) not null,
   `nama_ibu` varchar(100),
   `nama_ayah` varchar(50),
   `alamat` varchar(255) not null,
   `rt` char(3) not null,
   `rw` char(3) not null,
   `kel` varchar(50) not null,
   `kec` varchar(50) not null,
   `kota` varchar(50) not null,
   `pos` char(5) not null,
   `prop` varchar(50) not null,
   `kwn` tinyint(1) default 0,
   PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_penduduk` (`nik`, `no_kk`, `nama`, `jns_kel`, `tmpt_lahir`, `tgl_lahir`, `gol_darah`, `agama`, `status_kawin`, `status_kel`, `pendidikan`, `pekerjaan`, `nama_ibu`, `nama_ayah`, `alamat`, `rt`, `rw`, `kel`, `kec`, `kota`, `pos`, `prop`, `kwn`) VALUES
('3375823547687234', '3375826874475735', 'Mas Admin', 1, 'Pekalongan', '1983-08-25', 0, 0, 1, 0, 'Apa Ajah', 'Administrator Aplikasi', 'Bu Erwe', 'Pak Kades', 'Alamat Lengkap Pak Kades', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah', 0),
('3375823547687454', '3375826874475735', 'Bu Erwe', 0, 'Pekalongan', '1989-06-16', 3, 0, 1, 1, 'Apa Ajah', 'Ketua Erwe', 'Ibu Mertuanya Pak Kades', 'Ayah Mertuanya Pak Kades', 'Alamat Lengkap Pak Kades', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah', 0),
('3375823547687456', '3375826874475735', 'Pak Kades', 1, 'Pekalongan', '1984-05-14', 1, 0, 1, 0, 'Apa Ajah', 'Kepala Desa', 'Ibu Mertuanya Bu Erwe', 'Ayah Mertuanya Bu Erwe', 'Alamat Lengkap Pak Kades', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah', 0),
('3375823547687457', '3375826874475735', 'Mbak Erte', 0, 'Pekalongan', '1984-03-12', 2, 0, 1, 0, 'Apa Ajah', 'Ketua Erte', 'Bu Erwe', 'Pak Kades', 'Alamat Lengkap Pak Kades', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah', 0),
('3375823547687567', '3375826875476735', 'Nama Bapak', 1, 'Pekalongan', '1984-07-15', 1, 0, 1, 0, 'Apa Ajah', 'Bapak Rumah Tangga', 'Ibu Mertuanya Nama Ibu', 'Ayah Mertuanya Nama Ibu', 'Alamat Lengkap si Bapak', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah', 0),
('3375823547687787', '3375826875476735', 'Nama Ibu', 0, 'Pekalongan', '1983-06-16', 1, 0, 1, 1, 'Apa Ajah', 'Ibu Rumah Tangga', 'Ibu Mertuanya Nama Bapak', 'Ayah Mertuanya Nama Bapak', 'Alamat Lengkap si Bapak', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah', 0),
('3375823547787687', '3375826875476735', 'Nama Anak', 0, 'Pekalongan', '2005-06-16', 0, 0, 0, 2, 'Pendidikan', 'Pelajar', 'Nama Ibu', 'Nama Ayah', 'Alamat Lengkap si Bapak', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah', 0);

-- Update tbl_user structure

ALTER TABLE `tbl_pengguna` ADD `nik` char(16) not null AFTER `id`;

-- Add nik to Administrator

UPDATE `tbl_pengguna` SET `nik` = '3375823547687234' WHERE `tbl_pengguna`.`id` = 1;

-- Add Another pengguna

INSERT INTO `tbl_pengguna` (`nik`, `username`, `email`, `password`, `level`) VALUES
('3375823547687454', 'buerwe', 'buerwe@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 3),
('3375823547687456', 'pakkades', 'pakkades@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 2),
('3375823547687457', 'pakerte', 'pakerte@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 4),
('3375823547687567', 'bapakku', 'bapakku@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 5),
('3375823547687787', 'username', 'saya@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 5);

DROP TABLE IF EXISTS `tbl_keluarga`;
CREATE TABLE `tbl_keluarga` (
   `no_kk` char(16) not null,
   `nik_kepala` char(16) not null,
   `alamat` varchar(255) not null,
   `rt` char(3) not null,
   `rw` char(3) not null,
   `kel` varchar(50) not null,
   `kec` varchar(50) not null,
   `kota` varchar(50) not null,
   `pos` char(5) not null,
   `prop` varchar(50) not null,
   PRIMARY KEY (`no_kk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_keluarga` (`no_kk`, `nik_kepala`, `alamat`, `rt`, `rw`, `kel`, `kec`, `kota`, `pos`, `prop`) VALUES
('3375826874475735', '3375823547687456', 'Alamat Lengkap Pak Kades', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah'),
('3375826875476735', '3375823547687567', 'Alamat Lengkap si Bapak', '002', '001', 'Nama Kelurahan', 'Nama Kecamatan', 'Kab. Pekalongan', '54321', 'Jawa Tengah');
