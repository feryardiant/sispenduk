<?php

class WargaPdf extends Pdf
{
    public function header() {
        $this->setFont($this->conf['fontFamily'], 'B', 14);
        $this->cell(0, 15, trim($this->docTitle), 0, 0, 'C');
        $this->ln();
    }

    public function lapor(Db $resource) {
        $a = $this->contentWidth - (37 * 2);
        $columns = array(
            'nik'        => array('width' => 36, 'align' => 'C', 'data' => 'NIK'),
            'nama'       => array('width' => 80, 'align' => 'L', 'data' => 'Nama Lengkap'),
            'jns_kel'    => array('width' => 30, 'align' => 'C', 'data' => 'Jns. Kelamin', '_call' => function ($jns_kel) {
                return Warga::$prop['jns_kel'][$jns_kel];
            }),
            'pekerjaan'  => array('width' => 44, 'align' => 'L', 'data' => 'Pekerjaan'),
        );

        $this->docTitle('Rekapitulasi Data Penduduk');
        $this->addPage();
        $this->addTable($this->parseData($resource, $columns));

        return $this;
    }
}
