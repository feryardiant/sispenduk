<?php defined('ABSPATH') or die ('Not allowed!') ?>
<form action="<?php echo currentUrl() ?>" method="post" class="form">

    <div class="control-group">
        <label class="label" for="no_kk">No. KK</label>
        <div class="control-input">
            <input type="text" required name="no_kk" id="no_kk" <?php echo $id ? 'value="'.$data->no_kk.'"' : '' ?> autofocus>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="nik_kepala">Nama Kepala Keluarga</label>
        <div class="control-input">
            <input type="text" required name="nik_kepala" id="nik_kepala" <?php echo $id ? 'value="'.$data->nik_kepala.'"' : '' ?>>
        </div>
    </div>

    <?php include 'alamat.php' ?>

    <div class="control-group">
        <label class="label" for="kota">Data Keluarga</label>
        <div class="control-input">
            <button type="button" id="add-btn" class="btn">Tambah Data</button>
            <table id="t-keluarga" class="data">
                <thead>
                    <tr>
                        <th style="width:20%">NIK</th>
                        <th style="width:30%">Nama Lengkap</th>
                        <th style="width:20%">Status Dlm Keluarga</th>
                        <th class="action">Pilihan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="empty acenter"><td colspan="5">Belum ada data.</td></tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="form control-action">
        <input type="submit" name="submit" id="submit-btn" class="btn" value="Simpan">
        <input type="reset" id="cancel-btn" class="btn" value="Batal">
    </div>

</form>
<?php $status_kel = json_encode(Warga::$prop['status_kel']) ?>
<script>
$(function () {
    var tname = '#t-keluarga'
    var tbody = $(tname + ' > tbody')
    // var status = <?php echo $status_kel."\n" ?>
    var emptyTmpl = '<tr class="empty acenter"><td colspan="5">Belum ada data.</td></tr>'
    var fieldTmpl = [
        '<tr class="kel-row">',
        '<td><input type="text" name="keluarga_nik[]" placeholder="NIK"></td>',
        '<td><input type="text" name="keluarga_nama[]" placeholder="Nama Lengkap"></td>',
        '<td><select name="keluarga_status[]">'
    ]

    $.each(<?php echo $status_kel."\n" ?>, function (key, val) {
        fieldTmpl.push('<option value="' + key + '">' + val + '</option>')
    })

    fieldTmpl.push([
        '</select></td>',
        '<td class="action"><button type="button" class="btn remove-row-btn">Hapus</button></td>',
        '</tr>'
    ])

    $(tname).on('click', '.remove-row-btn', function () {
        $(this).parents('tr').remove()
        if ($('tbody > tr').size() === 0) {
            $(tname + ' > tbody').append(emptyTmpl)
        }
    })

    $('#add-btn').click(function () {
        if (tbody.children('tr').hasClass('empty')) {
            tbody.empty()
        }

        tbody.append(fieldTmpl.join())
        tbody.find('td:first input').focus()
    })
})
</script>
