<?php defined('ABSPATH') or die ('Not allowed!') ?>
<form action="<?php echo currentUrl() ?>" method="post" class="form">

    <div class="control-group">
        <label class="label" for="nik">NIK</label>
        <div class="control-input">
            <input type="text" required name="nik" id="nik" <?php echo $id ? 'value="'.$data->nik.'"' : '' ?> autofocus <?php echo $avail ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="no_kk">No. KK</label>
        <div class="control-input">
            <input type="text" required name="no_kk" id="no_kk" <?php echo $id ? 'value="'.$data->no_kk.'"' : '' ?> <?php echo $avail ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="nama">Nama Lengkap</label>
        <div class="control-input">
            <input type="text" required name="nama" id="nama" <?php echo $id ? 'value="'.$data->nama.'"' : '' ?> <?php echo $avail ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="jns_kel">Jenis Kelamin</label>
        <div class="control-input">
        <?php if (in_array($userLevel, array(1, 4))): ?>
            <?php foreach (Warga::$prop['jns_kel'] as $val => $jns_kel) : ?>
                <input type="radio" name="jns_kel" id="jns_kel-<?php echo $val ?>" value="<?php echo $val ?>" <?php echo $id && $data->jns_kel == $val ? 'checked' : '' ?>>
                <label for="jns_kel-<?php echo $val ?>"><?php echo $jns_kel ?></label>
            <?php endforeach ?>
        <?php else: ?>
            <p class="input"><?php echo Warga::$prop['jns_kel'][$data->jns_kel] ?></p>
        <?php endif ?>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="tmpt_lahir">Tempat/Tanggal Lahir</label>
        <div class="control-input">
            <input type="text" class="small" required name="tmpt_lahir" id="tmpt_lahir" <?php echo $id ? 'value="'.$data->tmpt_lahir.'"' : '' ?> <?php echo $avail ?>>
            <input type="text" class="small jqui-datepicker" required name="tgl_lahir" id="tgl_lahir" <?php echo $id ? 'value="'.$data->tgl_lahir.'"' : '' ?> <?php echo $avail ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="gol_darah">Golongan Darah</label>
        <div class="control-input">
        <?php if (in_array($userLevel, array(1, 4))): ?>
            <?php foreach (Warga::$prop['gol_darah'] as $val => $gol_darah) : ?>
                <input type="radio" name="gol_darah" id="gol_darah-<?php echo $val ?>" value="<?php echo $val ?>" <?php echo $id && $data->gol_darah == $val ? 'checked' : '' ?>>
                <label for="gol_darah-<?php echo $val ?>"><?php echo $gol_darah ?></label>
            <?php endforeach ?>
        <?php else: ?>
            <p class="input"><?php echo Warga::$prop['gol_darah'][$data->gol_darah] ?></p>
        <?php endif ?>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="agama">Agama</label>
        <div class="control-input">
        <?php if (in_array($userLevel, array(1, 4))): ?>
            <?php foreach (Warga::$prop['agama'] as $val => $agama) : ?>
                <input type="radio" name="agama" id="agama-<?php echo $val ?>" value="<?php echo $val ?>" <?php echo $id && $data->agama == $val ? 'checked' : '' ?>>
                <label for="agama-<?php echo $val ?>"><?php echo $agama ?></label>
            <?php endforeach ?>
        <?php else: ?>
            <p class="input"><?php echo Warga::$prop['agama'][$data->agama] ?></p>
        <?php endif ?>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="kwn">Kewarganegaraan</label>
        <div class="control-input">
        <?php if (in_array($userLevel, array(1, 4))): ?>
            <?php foreach (Warga::$prop['kwn'] as $val => $kwn) : ?>
                <input type="radio" name="kwn" id="kwn-<?php echo $val ?>" value="<?php echo $val ?>" <?php echo $id && $data->kwn == $val ? 'checked' : '' ?>>
                <label for="kwn-<?php echo $val ?>"><?php echo $kwn ?></label>
            <?php endforeach ?>
        <?php else: ?>
            <p class="input"><?php echo Warga::$prop['kwn'][$data->kwn] ?></p>
        <?php endif ?>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="status_kawin">Status Kawin</label>
        <div class="control-input">
        <?php if (in_array($userLevel, array(1, 4))): ?>
            <?php foreach (Warga::$prop['status_kawin'] as $val => $status_kawin) : ?>
                <input type="radio" name="status_kawin" id="status_kawin-<?php echo $val ?>" value="<?php echo $val ?>" <?php echo $id && $data->status_kawin == $val ? 'checked' : '' ?>>
                <label for="status_kawin-<?php echo $val ?>"><?php echo $status_kawin ?></label>
            <?php endforeach ?>
        <?php else: ?>
            <p class="input"><?php echo Warga::$prop['status_kawin'][$data->status_kawin] ?></p>
        <?php endif ?>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="status_kel">Status dalam keluarga</label>
        <div class="control-input">
        <?php if (in_array($userLevel, array(1, 4))): $i = 1; ?>
            <?php foreach (Warga::$prop['status_kel'] as $val => $status_kel) : ?>
                <input type="radio" name="status_kel" id="status_kel-<?php echo $val ?>" value="<?php echo $val ?>" <?php echo $id && $data->status_kel == $val ? 'checked' : '' ?>>
                <label for="status_kel-<?php echo $val ?>"><?php echo $status_kel ?></label>
            <?php echo ($i % 6 == 0) ? '<br>' : '' ?>
            <?php $i++; endforeach; ?>
        <?php else: ?>
            <p class="input"><?php echo Warga::$prop['status_kel'][$data->status_kel] ?></p>
        <?php endif ?>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="pendidikan">Pendidikan</label>
        <div class="control-input">
            <input type="text" required name="pendidikan" id="pendidikan" <?php echo $id ? 'value="'.$data->pendidikan.'"' : '' ?> <?php echo $avail ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="pekerjaan">Pekerjaan</label>
        <div class="control-input">
            <input type="text" required name="pekerjaan" id="pekerjaan" <?php echo $id ? 'value="'.$data->pekerjaan.'"' : '' ?> <?php echo $avail ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="nama_ibu">Nama Orang Tua</label>
        <div class="control-input">
            <input type="text" class="small" placeholder="Nama Ibu" required name="nama_ibu" id="nama_ibu" <?php echo $id ? 'value="'.$data->nama_ibu.'"' : '' ?> <?php echo $avail ?>>
            <input type="text" class="small" placeholder="Nama Ayah" required name="nama_ayah" id="nama_ayah" <?php echo $id ? 'value="'.$data->nama_ayah.'"' : '' ?> <?php echo $avail ?>>
        </div>
    </div>

    <?php include 'alamat.php' ?>

<?php if (in_array($userLevel, array(1, 4))): ?>
    <div class="form control-action">
        <input type="submit" name="submit" id="submit-btn" class="btn" value="Simpan">
        <input type="reset" id="cancel-btn" class="btn" value="Batal">
    </div>
<?php endif ?>

</form>
