<?php defined('ABSPATH') or die ('Not allowed!') ?>

<div class="control-group">
    <label class="label" for="alamat">Alamat</label>
    <div class="control-input">
        <input <?php echo $avail ?> type="text" name="alamat" id="alamat" value="<?php echo $id ? $data->alamat : Warga::$prop['alamat'] ?>" required placeholder="Alamat Lengkap">
    </div>
</div>

<div class="control-group">
    <label class="label" for="rt">RT/RW/Kode Pos</label>
    <div class="control-input">
        <input <?php echo $avail ?> type="text" name="rt" id="rt" value="<?php echo $id ? $data->rt : Warga::$prop['rt'] ?>" required class="mini" placeholder="Rt">
        <input <?php echo $avail ?> type="text" name="rw" id="rw" value="<?php echo $id ? $data->rw : Warga::$prop['rw'] ?>" required class="mini" placeholder="Rw">
        <input <?php echo $avail ?> type="text" name="pos" id="pos" value="<?php echo $id ? $data->pos : Warga::$prop['pos'] ?>" required class="small" placeholder="Kode Pos">
    </div>
</div>

<div class="control-group">
    <label class="label" for="kel">Kelurahan/Kecamatan</label>
    <div class="control-input">
        <input <?php echo $avail ?> type="text" name="kel" id="kel" value="<?php echo $id ? $data->kel : Warga::$prop['kel'] ?>" required class="small" placeholder="Kelurahan">
        <input <?php echo $avail ?> type="text" name="kec" id="kec" value="<?php echo $id ? $data->kec : Warga::$prop['kec'] ?>" required class="small" placeholder="Kecamatan">
    </div>
</div>

<div class="control-group">
    <label class="label" for="kota">Kota/Propinsi</label>
    <div class="control-input">
        <input <?php echo $avail ?> type="text" name="kota" id="kota" value="<?php echo $id ? $data->kota : Warga::$prop['kota'] ?>" required class="small" placeholder="Kabupaten/Kota">
        <input <?php echo $avail ?> type="text" name="prop" id="prop" value="<?php echo $id ? $data->prop : Warga::$prop['prop'] ?>" required class="small" placeholder="Propinsi">
    </div>
</div>
