<?php defined('ABSPATH') or die ('Not allowed!');

template('header');

$isProfile = $page == 'profil' ? true : false;
if ($isProfile) {
    $page = 'form';
}

$toolbars = array();
if ($userMod->is('admin')) {
    $toolbars['data']['?p=form'] = 'Baru';
}
$toolbars['data'][1]['?p=data'] = 'Semua';
$toolbars['form']['?p=data'] = 'Kembali';
foreach (User::$levels as $key => $val) {
    $toolbars['data'][1]['?p=data&level='.$key] = $val;
}

$judul = ucfirst($page);
?>
<header id="content-header" class="clearfix">
    <h3 id="page-title"><?php echo $judul ?> Pengguna</h3>
    <?php if ($userMod->loggedin()) echo Menu::toolbar($toolbars[$page]) ?>
</header>
<div id="content-main" class="clearfix">
    <?php if (file_exists($_page = dirname(__FILE__).DS.'incl'.DS.$page.EXT)) include $_page ?>
</div>
<?php template('footer') ?>
