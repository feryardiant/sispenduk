<?php defined('ABSPATH') or die ('Not allowed!');

$do = get('do');
$data = ($id = get('id')) ? $userMod->fetchOne($id, 'id') : false;
$canEdit = $userMod->is('admin') || (isset($data->username) && $userMod->current('username') == $data->username) ? true : false;
if ($isProfile) {
    $data = $userMod->data($userMod->current('username'), 'username');
}

if ($action = post('action')) {
    $return = false;
    $message = '';

    if ($action == 'login') {
        $return = $userMod->login();
    } elseif ($action == 'register') {
        try {
            $return = $userMod->save();
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
    }

    if ($return) {
        App::alert(ucfirst($action).' berhasil', 'success');
        $redir = get('redir') ?: '';
        if ($action == 'register') redirect($redir, 4);
        redirect($redir);
    } else {
        App::alert(ucfirst($action).' gagal. '.$message, 'error');
    }
} ?>

<form action="<?php echo currentUrl() ?>" id="<?php echo $do ? 'login' : 'user' ?>-form" method="post" class="form">
<?php if ($do): ?>
    <input type="hidden" name="action" value="<?php echo $do ?>">
<?php endif ?>
    <input type="hidden" name="level" value="5">
<?php if ($userMod->loggedin()): ?>
    <div class="control-group">
        <label class="label" for="nik">NIK</label>
        <div class="control-input">
        <?php if ($data): ?>
            <p class="input"><?php echo $data ? $data->nik : '' ?></p>
        <?php else: ?>
            <input type="text" required name="nik" id="nik">
        <?php endif ?>
        </div>
    </div>
<?php endif ?>
    <div class="control-group">
        <label class="label" for="username">Username</label>
        <div class="control-input">
        <?php if ($userMod->loggedin()): ?>
            <?php if ($canEdit): ?>
                <input type="text" required name="username" id="username" <?php echo $data ? 'value="'.$data->username.'"' : '' ?>>
            <?php else: ?>
                <p class="input"><?php echo $data ? $data->username : '' ?></p>
            <?php endif ?>
        <?php else: ?>
            <input type="text" required name="username" id="username">
        <?php endif ?>
        </div>
    </div>
    <input type="hidden" name="level" value="0">
<?php if ($userMod->loggedin()): ?>
    <div class="control-group">
        <label class="label" for="email">Email</label>
        <div class="control-input">
        <?php if ($canEdit): ?>
            <input type="email" required name="email" id="email" <?php echo $data ? 'value="'.$data->email.'"' : '' ?>>
        <?php else: ?>
            <p class="input"><?php echo $data ? $data->email : '' ?></p>
        <?php endif ?>
        </div>
    </div>
<?php endif ?>
<?php if (!$userMod->loggedin() || $canEdit): if ($do == 'login' || $canEdit): ?>
    <div class="control-group">
        <label class="label" for="password">Password</label>
        <div class="control-input">
            <input type="password" required name="password" id="password">
        </div>
    </div>
    <?php endif; if ($do == 'register' || $canEdit): ?>
    <div class="control-group">
        <label class="label" for="passconf">Ulangi Password</label>
        <div class="control-input">
            <input type="password" required name="pass_conf" id="pass_conf">
        </div>
    </div>
<?php endif; endif; ?>
<?php if ($userMod->loggedin()): ?>
    <div class="control-group">
        <label class="label" for="level">Level</label>
        <div class="control-input">
        <?php if ($userMod->is('admin')): ?>
            <select required name="level" id="level">
                <option>---</option>
                <?php foreach (User::$levels as $val => $level) : ?>
                    <option <?php echo ($id and $data->level == $val) ? 'selected' : '' ?> value="<?php echo $val ?>"><?php echo $level ?></option>
                <?php endforeach ?>
            </select>
        <?php else: ?>
            <p class="input"><?php echo $data ? User::$levels[$data->level] : '' ?></p>
        <?php endif ?>
        </div>
    </div>
<?php endif ?>
    <div class="form control-action">
    <?php if (!$userMod->loggedin()): ?>
        <?php if ($do == 'register'): ?>
            <input type="submit" id="submit-btn" class="btn" value="Kirim" autofocus>
            <?php echo anchor('?user=login', 'Login', array('class' => 'btn fright')) ?>
        <?php else: ?>
            <input type="submit" id="submit-btn" class="btn" value="Login">
            <?php echo anchor('?user=register', 'Registrasi', array('class' => 'btn fright')) ?>
        <?php endif ?>
    <?php else: ?>
        <input type="submit" id="submit-btn" class="btn" value="Simpan">
        <input type="reset" id="cancel-btn" class="btn" value="Batal">
    <?php endif ?>
    </div>

</form>
