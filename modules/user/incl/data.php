<?php defined('ABSPATH') or die ('Not allowed!');

if ($level = get('level')) {
    $query = $userMod->fetch($level, 'level');
} else {
    $query = $userMod->fetch();
}

$table = new Table($query, array(
    'username' => 'Username',
    'email'    => 'Email',
    'level'    => array(
        'label' => 'Level',
        'extra' => 'class="acenter"',
        '_call' => function ($_level) {
            return User::$levels[$_level];
        },
    ),
));

echo $table->generate();
