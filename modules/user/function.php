<?php defined('ABSPATH') or die ('Not allowed!');

class User extends Module
{
    public static
        $levels = array(
            1 => 'Administrator',
            2 => 'Aparat Desa',
            3 => 'Ketua RW',
            4 => 'Ketua RT',
            5 => 'Penduduk',
        ),
        $aliases = array(
            'admin'    => 1,
            'kades'    => 2,
            'rw'       => 3,
            'rt'       => 4,
            'penduduk' => 5,
        );

    /**
     * Class Constructor
     *
     * @param  array  $configs  Konfigurasi
     */
    public function __construct() {
        parent::__construct();
        $this->menu->add($this, array(
            'caps'  => array(1, 2, 3, 4),
            'title' => 'Pengguna',
            'order' => -1,
        ));

        $do = get('user');
        if ($do == 'logout') {
            App::dropSession();
            redirect();
        } elseif (in_array($do, array('login', 'register'))) {
            if ($this->loggedin()) redirect();
            redirect('user?p=form&do='.$do);
        }
    }

    public function loginPage() {
        return dirname(__FILE__).DS.'loginform'.EXT;
    }

    public function fetch($val = false, $key = '') {
        $where = array();
        if ($val !== false) {
            $key || $key = 'id';
            $where = array($key => $val);
        }

        return static::$db->select('tbl_pengguna', '', $where);
    }

    public function fetchOne($val = false, $key = '') {
        if ($query = $this->fetch($val, $key)) {
            return $query->fetchOne();
        }

        return false;
    }

    public function loggedin() {
        return (bool) App::session('auth') !== false;
    }

    public function delete() {}

    public function is($alias) {
        if (!isset(self::$aliases[$alias])) return false;
        return (int) self::$aliases[$alias] == (int) App::session('level');
    }

    public function current($key) {
        return App::session($key);
    }

    public function menu() {
        $menu = '';

        if ($this->loggedin()) {
            $menu .= '<strong>Hallo, '.User::current('username').'</strong> '
                  .  anchor('?user=logout', 'Logout');
        } else {
            $menu .= anchor('?user=login', 'Akun Saya');
        }

        return $menu;
    }

    public function login() {
        $query = static::$db->select('tbl_pengguna', '', array(
            'username' => post('username')
        ));

        if ($query && ($logindata = $query->fetchOne()) && $logindata->password == md5(post('password'))) {
            $warga = Warga::get('penduduk', $logindata->nik, 'nik');
            App::session(array(
                'auth'     => 1,
                'nik'      => $warga->nik,
                'nama'     => $warga->nama,
                'username' => $logindata->username,
                'level'    => $logindata->level,
            ));

            return true;
        }

        return false;
    }

    public function getAlias($level) {
        return array_search($level, self::$aliases);
    }

    public function simpan() {
        if (!($warga = Warga::get('penduduk', post('nik'), 'nik'))) {
            App::error('NIK yang anda masukan bukan warga desa ini.');
        }

        if (($user = self::fetch($warga->nik, 'nik')->fetchOne()) || $user->username == post('username')) {
            App::error('Pengguna dengan data tersebut sudah ada.');
        }

        $data = array(
            'nik'      => (int) $warga->nik,
            'username' => post('username'),
            'email'    => post('email'),
            'level'    => (int) post('level'),
        );

        if ($pass = post('password')) {
            $data['password'] = md5($pass);
        }

        $term = ($id = get('id')) ? array('id' => $id) : array();

        return static::$db->save('tbl_pengguna', $data, $term);
    }
}
