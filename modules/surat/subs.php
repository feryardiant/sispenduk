<?php defined('ABSPATH') or die ('Not allowed!');

return array(
    'sk' => array(
        'title' => 'Surat Keterangan',
        'subs' => array(
            'sk-umum' => 'Surat Keterangan Umum',
            'sk-takmampu' => 'Surat Keterangan Tidak Mampu',
            'sk-usaha' => 'Surat Keterangan Usaha',
            'sk-domtinggal' => 'Surat Keterangan Domisili Tempat Tinggal',
            'sk-domusaha' => 'Surat Keterangan Domisili Usaha',
        ),
    ),
    'sp' => array(
        'title' => 'Surat Pengantar',
        'subs' => array(
            'spengu' => 'Surat Pengantar Umum',
            'speru' => 'Surat Pernyataan Umum',
            'spemu' => 'Surat Pemberitahuan Umum',
            'spengpol' => 'Surat Pengantar Catatan Kepolisian',
            'spengho' => 'Surat Pengantar Ijin Keramaian',
        ),
    ),
    'capil' => array(
        'title' => 'DUKCAPIL',
        'subs' => array(
            'sk-lahir' => 'Surat Keterangan Kelahiran',
            'sp-ktp' => 'Surat Permohonan KTP',
            'sp-kk' => 'Surat Permohonan KK',
            'sp-pindah' => 'Surat Permohonan Pindah',
            'sk-mati' => 'Surat Keterangan Kematian',
        ),
    ),
);
