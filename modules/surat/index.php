<?php defined('ABSPATH') or die('Not allowed!');

if (!$userMod->loggedin()) {
    redirect('../?user=login');
}

$url = str_replace(ABSPATH, siteUrl(), dirname(__FILE__));
$id = get('id');
$sub2 = App::uriSegment(3);
$toolbars = array();

if ($sub1 = App::uriSegment(2)) {
    if (
        !isset(Surat::$subs[$sub1]) ||
        ($sub2 && !isset(Surat::$subs[$sub1]['subs'][$sub2]))
    ) {
        App::show404();
    }

    $judul = ucfirst($page == 'pdf' ? 'Rekap' : $page) . ' ' . ($sub2 != '' ? Surat::$subs[$sub1]['subs'][$sub2] : Surat::$subs[$sub1]['title']);
} else {
    $judul = 'Semua Data Surat';
}

template('header');

if ($sub2 != '') {
    $userLevel = $userMod->current('level');
    $curLevel = $userMod->getAlias($userLevel);
    $prevLevel = $userMod->getAlias($userLevel + 1);
    $_lvlStat = 'status_' . $curLevel;

    if ($page == 'form') {
        if (post('submit')) {
            if ($ret = $suratMod->save()) {
                $message = 'Surat permohonan anda berhasil disimpan' .
                ($userLevel == 5 ? ' dan akan segera ditindaklanjuti. Terima kasih' : '');
                App::alert($message, 'success');
            } else {
                App::alert('Terjadi masalah saat proses penyimpanan surat.', 'error');
            }
        }

        $backlink = $userLevel == 5 ? 'surat' : '?p=data';
        $toolbars[$backlink] = 'Kembali';
        $dataNik = null;

        if ($id) {
            $data = $suratMod->get($id);
            $dataNik = $data->nik;
            $docLink = '?p=form&id=' . $id;
            $toolbars['?p=pdf&id=' . $id] = 'Cetak PDF';

            if ($do = get('do')) {
                if ($suratMod->changeStatus($do, $curLevel, $id)) {
                    App::alert('Dokumen ini berhasil di' . $do, 'success');
                    redirect($docLink, 3);
                } else {
                    App::alert('Terjadi masalah saat proses penyimpanan surat.', 'error');
                }
            }

            if (
                !in_array($userLevel, array(1, 5)) &&
                ($curLevel != 'rt' && $data->{'status_' . $prevLevel} > 0) ||
                ($curLevel == 'rt' && $data->$_lvlStat == 0)
            ) {
                if ($data->status_kades == 0) {
                    foreach (array('tolak', 'setujui', 'hapus') as $action) {
                        $toolbars[1][$docLink . '&do=' . $action] = ucfirst($action);
                    }
                }
            }
        }

        // $warga = Warga::get('penduduk', $dataNik ?: $userMod->current('nik'), 'nik');
        $file = 'form/' . ($sub1 == 'sk' ? $sub1 : $sub2);
    } else {
        $toolbars = array('?p=pdf' => 'Export PDF');
        if (!in_array($userLevel, array(2, 3))) {
            $toolbars['?p=form'] = 'Baru';
        }

        foreach (Surat::$status as $key => $stat) {
            $toolbars[1]['?status=' . $key] = ucfirst($stat);
        }

        $file = 'data';
    }
} ?>
<header id="content-header" class="clearfix">
    <h3 id="page-title"><?php echo $judul?></h3>
<?php echo Menu::toolbar($toolbars)?>
</header>
<div id="content-main" class="clearfix">
<?php if (isset($file)):
    if ($page != 'form'):
        include $file . '.php';
    else:?>
	    <script type="text/javascript" src="<?php echo $url?>/asset/js/script.js"></script>
	    <form action="<?php echo currentUrl()?>" method="post" class="form">
	<?php include $file . '.php'?>
	</form>
	<?php endif;
else:
    echo $suratMod->tiles();
endif;?>
</div>
<?php template('footer')?>
