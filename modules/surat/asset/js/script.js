$(document).ready(function () {
    var siteUrl = $('body').data('siteurl')
    var autoNik = 'input[data-autocomplete="nik"]'

    $('.toolbar-btn').each(function () {
        var btnId = $(this).attr('id')
        var actBtn = [ 'tolak-btn', 'setujui-btn', 'hapus-btn' ]

        if ($(btnId) !== undefined && actBtn.indexOf(btnId) != -1 ) {
            $(this).click(btnId, function (e) {
                if (!confirm('Apakah anda yakin?')) {
                    e.preventDefault();
                }
            })
        }
    })

    $.fn.nikAutocomplete = function (selectRes) {
        var penduduk;

        $(this).autocomplete({
            minLength: 10,
            source: function (req, res) {
                $.get(siteUrl + 'warga', {
                    table: 'penduduk',
                    nik: req.term
                }, function (data) {
                    var result = []
                    for (var row in data) {
                        result[row] = data[row].nik
                    }
                    penduduk = data[row]
                    res(result)
                })
            },
            select: function () {
                selectRes(penduduk)
            }
        })
    }

    $.fn.tableExp = function (emptyTmpl, rowTmpl) {
        var tname = '#' + $(this).attr('id')
        var tbody = $(tname + ' > tbody')

        $(tname).on('click', '.remove-row-btn', function () {
            $(this).parents('tr').remove()
            if ($('tbody > tr').size() === 0) {
                tbody.append(emptyTmpl)
            }
        })

        $('#add-btn').click(function () {
            if (tbody.children('tr').hasClass('empty')) {
                tbody.empty()
            }

            tbody.append(rowTmpl().join())
            tbody.find('tr:last td:first input').focus()
            tbody.on('keydown', 'input[name="keluarga_nik[]"]', function () {
                var nikEl = $(this)
                var trow  = nikEl.parents('tr.kel-row')

                nikEl.nikAutocomplete(function (penduduk) {
                    trow.find('input[name="keluarga_nama[]"]').val(penduduk.nama)
                    trow.find('select[name="keluarga_shdk[]"] option[value="' + penduduk.status_kel + '"]').attr("selected","selected");
                })
            })
        })
    }

    if ($(autoNik) !== undefined) {
        $(autoNik).nikAutocomplete(function (penduduk) {
            var nikElId = $(this).attr('id');

            // event, ui
            if (typeof penduduk == 'object') {
                $.each(penduduk, function (key, value) {
                    if (nikElId == 'nik_pemohon') {
                        $('dd#' + key).html(value)
                    } else {
                        var rel = nikElId.substring(4)
                        var input = '[name=' + key + '_' + rel + ']'

                        if ($(input).is(':text')) {
                            $(input).val(value)
                        } else if ($(input).is(':radio') || $(input).is(':checkbox')) {
                            $('#' + key + '_' + rel + '-' + value).attr('checked', 'checked')
                        }
                    }
                })
            }
        })
    }
})
