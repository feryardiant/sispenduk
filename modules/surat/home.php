<?php defined('ABSPATH') or die ('Not allowed!');

if (!$userMod->loggedin()) redirect('../?user=login');

if (($userLevel = $userMod->current('level')) && !in_array($userLevel, array(5, 1))):
    $i = 0;
    $levelAlias = $userMod->getAlias($userLevel); ?>
    <div class="clearfix">
    <?php foreach ($suratMod->child() as $type => $prop) : ?>
        <div style="width: 450px; float: left; margin: 0 7px 10px;">
        <?php $table = new Table($suratMod->fetch(array('status_'.$levelAlias => 0, 'jns_surat' => $type)), array(
            'id no_surat tgl_surat' => array('width' => 30, 'label' => 'Nomor &amp; Tanggal Surat', '_call' => function ($id, $no_surat, $tgl_surat) use ($prop) {
                return 'No: '.anchor($prop['url'].'?p=form&id='.$id, $no_surat)
                    .'<br>Tanggal: '.formatTanggal($tgl_surat);
            }),
            'nik nama' => array('width' => 30, 'label' => 'NIK &amp; Nama Pemohon', '_frmt' => 'NIK: %s<br>Nama: %s'),
            'tujuan'   => array('width' => 25, 'label' => 'Keperluan'),
        ));
        $table->noButtons = true;
        $table->setCaption(anchor($prop['url'], $prop['label'].' &rarr;'), 'style="margin-bottom: 5px;"');

        echo $table->generate();
        $i++; ?>
        </div>
        <?php if ($i % 2 == 0): ?>
            </div><div class="clearfix">
        <?php endif ?>
    <?php endforeach; ?>
    </div>
<?php else:
    $_lvlStat = in_array($userLevel, array(1, 5)) ? 'status_kades' : $_lvlStat;
    $columns = array(
        'no_surat'  => array('width' => 13, 'label' => 'Nomor Surat'),
        'tgl_surat' => array('width' => 10, 'label' => 'Tanggal Surat', '_call' => 'formatTanggal'),
        'nik'       => array('width' => 13, 'label' => 'NIK Pemohon'),
        'nama'      => array('width' => 17, 'label' => 'Nama Pemohon'),
        'tujuan'    => array('width' => 25, 'label' => 'Keperluan'),
        $_lvlStat   => array('width' => 10, 'label' => 'Status', '_call' => function ($_lvlStat) {
            return ucfirst(Surat::$status[$_lvlStat]);
        }, 'extra' => 'class="acenter"'),
    );

    $table = new Table($suratMod->fetch($userMod->current('nik'), 'nik_pemohon'), $columns);

    echo $table->generate();
endif; ?>
