<?php defined('ABSPATH') or die ('Not allowed!');

if ($userMod->current('level') == 5) {
    redirect('?p=form');
}

$_lvlStat    = in_array($userLevel, array(1, 5)) ? 'status_kades' : $_lvlStat;
$del_message = 'Apakah anda yakin ingin menghapus data tersebut.';
$columns     = array(
    'no_surat'  => array('width' => 13, 'label' => 'Nomor Surat'),
    'tgl_surat' => array('width' => 10, 'label' => 'Tanggal Surat', '_call' => 'formatTanggal'),
    'nik'       => array('width' => 13, 'label' => 'NIK Pemohon'),
    'nama'      => array('width' => 17, 'label' => 'Nama Pemohon'),
    'tujuan'    => array('width' => 25, 'label' => 'Keperluan'),
    $_lvlStat   => array('width' => 10, 'label' => 'Status', '_call' => function ($_lvlStat) {
            return ucfirst(Surat::$status[$_lvlStat]);
        }, 'extra' => 'class="acenter"'),
);

$table = new Table($suratMod->fetch(), $columns);
// $table->rowNum = true;

echo $table->generate();
