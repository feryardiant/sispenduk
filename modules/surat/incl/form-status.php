<?php defined('ABSPATH') or die ('Not allowed!') ?>

<?php if (!in_array($userLevel, array(1, 5))): ?>
    <div class="control-group">
        <label class="label" for="status">Status</label>
        <div class="control-input">
            <p class="input"><?php
if ($curStat = $data->$_lvlStat) {
    echo ucfirst(Surat::$status[$curStat]).' oleh anda dan menunggu proses selanjutnya.';
} else {
    echo 'Menunggu persetujuan '.($userLevel != 4 && $data->{'status_'.$prevLevel} == 0 ? strtoupper($prevLevel) : 'anda' ).'.';
}
            ?></p>
        </div>
    </div>
<?php else: ?>
    <input type="hidden" name="status_rt" value="0">
    <input type="hidden" name="status_rw" value="0">
    <input type="hidden" name="status_kades" value="0">
<?php endif ?>
