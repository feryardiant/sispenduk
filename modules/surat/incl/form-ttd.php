<?php defined('ABSPATH') or die ('Not allowed!') ?>

<?php if ($userLevel == 2): ?>
    <div class="control-group">
        <label class="label" for="ttd">Pejabat Terkait</label>
        <div class="control-input">
            <input type="text" required name="ttd" id="ttd" value="<?php echo $id && isset($data->ttd) ? $data->ttd : '' ?>">
        </div>
    </div>
<?php else: ?>
    <input type="hidden" name="ttd" value="Kepala Desa">
<?php endif ?>
