<?php defined('ABSPATH') or die ('Not allowed!') ?>

<div class="control-group">
    <label class="label" for="no_surat">Nomor Surat</label>
    <div class="control-input">
        <input type="text" required name="no_surat" id="no_surat" value="<?php echo $id ? $data->no_surat : $suratMod->getNo() ?>" readonly>
    </div>
</div>

<?php include 'form-penduduk'.EXT ?>
<?php include 'form-status'.EXT ?>
