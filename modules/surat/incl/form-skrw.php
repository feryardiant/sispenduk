<?php defined('ABSPATH') or die ('Not allowed!') ?>

<?php if ($userLevel == 3): ?>
    <div class="control-group">
        <label class="label" for="sk_rw_no">Surat Keterangan RW</label>
        <div class="control-input">
            <input type="text" class="mini" placeholder="Rw" name="sk_rw" id="sk_rw" <?php echo $id ? 'value="'.$data->sk_rw.'"' : '' ?>>
            <input type="text" class="mini" placeholder="Nomor Surat Keterangan" name="sk_rw_no" id="sk_rw_no" <?php echo $id ? 'value="'.$data->sk_rw_no.'"' : '' ?>>
            <input type="text" class="jqui-datepicker small" placeholder="Tanggal Surat Keterangan" name="sk_rw_tgl" id="sk_rw_tgl" <?php echo $id ? 'value="'.$data->sk_rw_tgl.'"' : '' ?>>
        </div>
    </div>
<?php else: ?>
    <input type="hidden" name="sk_rw" value="">
    <input type="hidden" name="sk_rw_no" value="">
    <input type="hidden" name="sk_rw_tgl" value="">
<?php endif ?>
