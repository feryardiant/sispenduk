<?php defined('ABSPATH') or die ('Not allowed!');

foreach ($tabs as $rel): ?>
    <div id="tab-<?php echo $rel ?>">
        <div class="control-group">
            <label class="label" for="nik_<?php echo $rel ?>">NIK</label>
            <div class="control-input">
                <input type="text" required data-autocomplete="nik" name="nik_<?php echo $rel ?>" id="nik_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'nik_'.$rel}.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="nama">Nama Lengkap</label>
            <div class="control-input">
                <input type="text" required name="nama_<?php echo $rel ?>" id="nama_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'nama_'.$rel}.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="tmpt_lahir">Tanggal Lahir</label>
            <div class="control-input">
                <input type="text" required name="tgl_lahir_<?php echo $rel ?>" id="tgl_lahir_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'tgl_lahir_'.$rel}.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="pekerjaan">Pekerjaan</label>
            <div class="control-input">
                <input type="text" required name="pekerjaan_<?php echo $rel ?>" id="pekerjaan_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'pekerjaan_'.$rel}.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="alamat">Alamat</label>
            <div class="control-input">
                <input type="text" required placeholder="Alamat Lengkap" name="alamat_<?php echo $rel ?>" id="alamat_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'alamat_'.$rel}.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="rt">RT/RW/Kode Pos</label>
            <div class="control-input">
                <input type="text" class="mini" placeholder="Rt" required name="rt_<?php echo $rel ?>" id="rt_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'rt_'.$rel}.'"' : '' ?>>
                <input type="text" class="mini" placeholder="Rw" required name="rw_<?php echo $rel ?>" id="rw_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'rw_'.$rel}.'"' : '' ?>>
                <input type="text" class="small" placeholder="Kode Pos" required name="pos_<?php echo $rel ?>" id="pos_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'pos_'.$rel}.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="kel">Kelurahan/Kecamatan</label>
            <div class="control-input">
                <input type="text" class="small" placeholder="Kelurahan" required name="kel_<?php echo $rel ?>" id="kel_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'kel_'.$rel}.'"' : '' ?>>
                <input type="text" class="small" placeholder="Kecamatan" required name="kec_<?php echo $rel ?>" id="kec_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'kec_'.$rel}.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="kota">Kota/Propinsi</label>
            <div class="control-input">
                <input type="text" class="small" placeholder="Kabupaten/Kota" required name="kota_<?php echo $rel ?>" id="kota_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'kota_'.$rel}.'"' : '' ?>>
                <input type="text" class="small" placeholder="Propinsi" required name="prop_<?php echo $rel ?>" id="prop_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'prop_'.$rel}.'"' : '' ?>>
            </div>
        </div>

    <?php if (in_array($rel, array('ibu', 'ayah'))): ?>
        <div class="control-group">
            <label class="label" for="kwn">Kewarganegaraan</label>
            <div class="control-input">
            <?php foreach (Warga::$prop['kwn'] as $val => $kwn) : ?>
                <input type="radio" name="<?php echo 'kwn_'.$rel ?>" id="<?php echo 'kwn_'.$rel.'-'.$val ?>" value="<?php echo $val ?>" <?php echo $id && $data->{'kwn_'.$rel} == $val ? 'checked' : '' ?>>
                <label for="<?php echo 'kwn_'.$rel.'-'.$val ?>"><?php echo $kwn ?></label>
            <?php endforeach ?>
            </div>
        </div>
    <?php endif; if ($rel == 'ibu'): ?>
        <div class="control-group">
            <label class="label" for="kebangsaan">Kebangsaan</label>
            <div class="control-input">
                <input type="text" required name="kebangsaan_<?php echo $rel ?>" id="kebangsaan_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'kebangsaan_'.$rel}.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="tgl_nikah">Tanggal Catat Nikah</label>
            <div class="control-input">
                <input type="text" class="jqui-datepicker" required name="tgl_nikah_<?php echo $rel ?>" id="tgl_nikah_<?php echo $rel ?>" <?php echo $id ? 'value="'.$data->{'tgl_nikah_'.$rel}.'"' : '' ?>>
            </div>
        </div>
    <?php endif ?>
    </div>
<?php endforeach ?>
