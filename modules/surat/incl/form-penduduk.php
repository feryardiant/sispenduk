<?php defined('ABSPATH') or die ('Not allowed!');

$nik = $id ? $data->nik : $userMod->current('nik');
$warga = $wargaMod->get('penduduk', $nik, 'nik'); ?>

<div class="control-group">
    <label class="label" for="nik_pemohon">NIK Pemohon</label>
    <div class="control-input">
    <?php $avail = ($userLevel == 5 || $id) ? 'readonly' : 'autofocus data-autocomplete="nik"' ?>
        <input type="text" required name="nik_pemohon" id="nik_pemohon" value="<?php echo $warga->nik ?>" <?php echo $avail ?>>
    </div>
</div>

<dl class="data-penduduk">
    <!-- Nama Lengkap -->
    <dt>Nama Lengkap</dt>
    <dd id="dd-warga-nama"><?php echo $warga->nama ?></dd>
    <!-- Jenis Kelamin -->
    <dt>Jenis Kelamin</dt>
    <dd id="dd-warga-jnskel"><?php echo Warga::$prop['jns_kel'][$warga->jns_kel] ?></dd>
    <!-- Tempat Lahir -->
    <dt>Tempat &amp; Tgl. Lahir</dt>
    <dd>
        <span id="dd-warga-tmptlahir"><?php echo $warga->tmpt_lahir ?></span>
        <span id="dd-warga-tgllahir"><?php echo formatTanggal($warga->tgl_lahir) ?></span>
    </dd>
    <!-- Kewarganegaraan -->
    <dt>Kewarganegaraan</dt>
    <dd id="dd-warga-kwn"><?php echo Warga::$prop['kwn'][$warga->kwn] ?></dd>
    <!-- Agama -->
    <dt>Agama</dt>
    <dd id="dd-warga-agama"><?php echo Warga::$prop['agama'][$warga->agama] ?></dd>
    <!-- Status Kawin -->
    <dt>Status Kawin</dt>
    <dd id="dd-warga-statuskawin"><?php echo Warga::$prop['status_kawin'][$warga->status_kawin] ?></dd>
    <!-- Pendidikan -->
    <dt>Pendidikan</dt>
    <dd id="dd-warga-pendidikan"><?php echo $warga->pendidikan ?></dd>
    <!-- Pekerjaan -->
    <dt>Pekerjaan</dt>
    <dd id="dd-warga-pekerjaan"><?php echo $warga->pekerjaan ?></dd>
</dl>
