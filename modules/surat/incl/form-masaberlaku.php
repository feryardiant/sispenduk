<?php defined('ABSPATH') or die ('Not allowed!') ?>

<?php if ($userLevel != 5): ?>
    <div class="control-group">
        <label class="label" for="berlaku">Berlaku</label>
        <div class="control-input">
            <input type="text" required name="berlaku" id="berlaku" <?php echo $id && isset($data->berlaku) ? 'value="'.$data->berlaku.'"' : '' ?>>
        </div>
    </div>
<?php else: ?>
    <input type="hidden" name="berlaku" value="">
<?php endif ?>
