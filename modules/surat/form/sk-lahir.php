<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT;
include dirname(__FILE__).'/../incl/form-ttd'.EXT; ?>

<div class="control-group">
    <label class="label" for="hub">Hubungan dengan bayi</label>
    <div class="control-input">
        <input type="text" required name="hub_pemohon" id="hub_hub_pemohon" <?php echo $id ? 'value="'.$data->hub_pemohon.'"' : '' ?>>
    </div>
</div>

<?php $tabs = array('ibu', 'ayah', 'saksi_1', 'saksi_2') ?>
<div class="jqui-tabs">
    <ul>
        <li><a href="#tab-bayi">Biodata Bayi/Anak</a></li>
    <?php foreach ($tabs as $rel): ?>
        <?php $relLabel = $rel == 'ibu' ? $rel.' Kandung' : str_replace('_', ' ', $rel); ?>
        <li><a href="#tab-<?php echo $rel ?>"><?php echo ucfirst($relLabel) ?></a></li>
    <?php endforeach ?>
    </ul>

    <div id="tab-bayi">
        <div class="control-group">
            <label class="label" for="nama_bayi">Nama Lengkap</label>
            <div class="control-input">
                <input type="text" required name="nama_bayi" id="nama_bayi" <?php echo $id ? 'value="'.$data->nama_bayi.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="jns_kel_bayi">Jenis Kelamin</label>
            <div class="control-input">
            <?php foreach (Warga::$prop['jns_kel'] as $val => $jns_kel_bayi) : ?>
                <input type="radio" name="jns_kel_bayi" id="jns_kel_bayi-<?php echo $val ?>" value="<?php echo $val ?>" <?php echo $id && $data->jns_kel_bayi == $val ? 'checked' : '' ?>>
                <label for="jns_kel_bayi-<?php echo $val ?>"><?php echo $jns_kel_bayi ?></label>
            <?php endforeach ?>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="tmpt_lahir_bayi">Tempat Dilahirkan</label>
            <div class="control-input">
                <input type="text" required name="tmpt_lahir_bayi" id="tmpt_lahir_bayi" <?php echo $id ? 'value="'.$data->tmpt_lahir_bayi.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="hari_lahir_bayi">Hari, Jam &amp; Tanggal Lahir</label>
            <div class="control-input">
                <input type="text" class="mini" required placeholder="Hari" name="hari_lahir_bayi" id="hari_lahir_bayi" <?php echo $id ? 'value="'.$data->hari_lahir_bayi.'"' : '' ?>>
                <input type="text" class="mini" required placeholder="Jam" name="waktu_lahir_bayi" id="waktu_lahir_bayi" <?php echo $id ? 'value="'.$data->waktu_lahir_bayi.'"' : '' ?>>
                <input type="text" class="small jqui-datepicker" required placeholder="Tanggal" name="tgl_lahir_bayi" id="tgl_lahir_bayi" <?php echo $id ? 'value="'.$data->tgl_lahir_bayi.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="jns_lahir_bayi">Jenis Kelahiran</label>
            <div class="control-input">
                <input type="text" required name="jns_lahir_bayi" id="jns_lahir_bayi" <?php echo $id ? 'value="'.$data->jns_lahir_bayi.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="urutan_bayi">Kelahiran Ke-</label>
            <div class="control-input">
                <input type="number" required name="urutan_bayi" id="urutan_bayi" <?php echo $id ? 'value="'.$data->urutan_bayi.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="asisten_lahir">Penolong Kelahiran</label>
            <div class="control-input">
                <input type="text" required name="asisten_lahir" id="asisten_lahir" <?php echo $id ? 'value="'.$data->asisten_lahir.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="berat_bayi">Berat &amp; Panjang Bayi)</label>
            <div class="control-input">
                <input type="number" required class="small" placeholder="Berat (Kg)" name="berat_bayi" id="berat_bayi" <?php echo $id ? 'value="'.$data->berat_bayi.'"' : '' ?>>
                <input type="number" required class="small" placeholder="Panjang (Cm)" name="panjang_bayi" id="panjang_bayi" <?php echo $id ? 'value="'.$data->panjang_bayi.'"' : '' ?>>
            </div>
        </div>
    </div>

    <?php include dirname(__FILE__).'/../incl/form-lahirmati'.EXT ?>
</div>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
