<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT; ?>

<div class="control-group">
    <label class="label" for="tujuan_pernyataan">Surat Pernyataan</label>
    <div class="control-input">
        <input type="text" required name="tujuan_pernyataan" id="tujuan_pernyataan" <?php echo $id ? 'value="'.$data->tujuan_pernyataan.'"' : '' ?>>
    </div>
</div>

<div class="control-group">
    <label class="label" for="isi_pernyataan">Isi Pernyataan</label>
    <div class="control-input clearfix">
        <textarea type="text" required name="isi_pernyataan" id="isi_pernyataan"><?php echo $id ? $data->isi_pernyataan : '' ?></textarea>
    </div>
</div>

<div class="control-group">
    <label class="label" for="saksi_1">Saksi 1 &amp; 2</label>
    <div class="control-input">
        <input type="text" class="small" required placeholder="Nama Saksi 1" name="saksi_1" id="saksi_1" <?php echo $id ? 'value="'.$data->saksi_1.'"' : '' ?>>
        <input type="text" class="small" required placeholder="Nama Saksi 2" name="saksi_2" id="saksi_2" <?php echo $id ? 'value="'.$data->saksi_2.'"' : '' ?>>
    </div>
</div>

<div class="control-group">
    <label class="label" for="ketua_rt">Ketua Rt &amp; Rw</label>
    <div class="control-input">
        <input type="text" class="small" required placeholder="Ketua Rt" name="ketua_rt" id="ketua_rt" <?php echo $id ? 'value="'.$data->ketua_rt.'"' : '' ?>>
        <input type="text" class="small" required placeholder="Ketua Rw" name="ketua_rw" id="ketua_rw" <?php echo $id ? 'value="'.$data->ketua_rw.'"' : '' ?>>
    </div>
</div>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
