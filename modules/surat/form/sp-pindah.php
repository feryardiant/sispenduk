<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT;
include dirname(__FILE__).'/../incl/form-ttd'.EXT; ?>

<div class="jqui-tabs">
    <ul>
        <li><a href="#tab-dari">Daerah Asal</a></li>
        <li><a href="#tab-pindah">Data Pindah</a></li>
        <li><a href="#tab-keluarga">Anggota Keluarga</a></li>
    </ul>

    <div id="tab-dari">
        <div class="control-group">
            <label class="label" for="no_kk_dari">Nomor KK</label>
            <div class="control-input">
                <input type="text" required name="no_kk_dari" id="no_kk_dari" <?php echo $id ? 'value="'.$data->no_kk_dari.'"' : '' ?> autofocus>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="nama_kk_dari">Nama Kepala Keluarga</label>
            <div class="control-input">
                <input type="text" required name="nama_kk_dari" id="nama_kk_dari" <?php echo $id ? 'value="'.$data->nama_kk_dari.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="alamat_dari">Alamat</label>
            <div class="control-input">
                <input type="text" required name="alamat_dari" id="alamat_dari" <?php echo $id ? 'value="'.$data->alamat_dari.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="rt_dari">RT/RW/Kode Pos</label>
            <div class="control-input">
                <input type="text" class="mini" placeholder="Rt" required name="rt_dari" id="rt_dari" <?php echo $id ? 'value="'.$data->rt_dari.'"' : '' ?>>
                <input type="text" class="mini" placeholder="Rw" required name="rw_dari" id="rw_dari" <?php echo $id ? 'value="'.$data->rw_dari.'"' : '' ?>>
                <input type="text" class="small" placeholder="Kode Pos" required name="pos_dari" id="pos_dari" <?php echo $id ? 'value="'.$data->pos_dari.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="nik_dari">NIK Pemohon</label>
            <div class="control-input">
                <input type="text" required name="nik_dari" id="nik_dari" value="<?php echo $id ? $data->nik_dari : $warga->nik ?>" autofocus>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="nama_dari">Nama Lengkap Pemohon</label>
            <div class="control-input">
                <input type="text" required name="nama_dari" id="nama_dari" value="<?php echo $id ? $data->nama_dari : $warga->nama ?>" autofocus>
            </div>
        </div>
    </div>

    <div id="tab-pindah">
        <div class="control-group">
            <label class="label" for="alasan_pindah">Alasan Pindah</label>
            <div class="control-input">
                <input type="text" required name="alasan_pindah" id="alasan_pindah" <?php echo $id ? 'value="'.$data->alasan_pindah.'"' : '' ?> autofocus>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="alamat_pindah">Alamat</label>
            <div class="control-input">
                <input type="text" required placeholder="Alamat Lengkap" name="alamat_pindah" id="alamat_pindah" <?php echo $id ? 'value="'.$data->alamat_pindah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="rt_pindah">RT/RW/Kode Pos</label>
            <div class="control-input">
                <input type="text" class="mini" placeholder="Rt" required name="rt_pindah" id="rt_pindah" <?php echo $id ? 'value="'.$data->rt_pindah.'"' : '' ?>>
                <input type="text" class="mini" placeholder="Rw" required name="rw_pindah" id="rw_pindah" <?php echo $id ? 'value="'.$data->rw_pindah.'"' : '' ?>>
                <input type="text" class="small" placeholder="Kode Pos" required name="pos_pindah" id="pos_pindah" <?php echo $id ? 'value="'.$data->pos_pindah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="kel_pindah">Kelurahan/Kecamatan</label>
            <div class="control-input">
                <input type="text" class="small" placeholder="Kelurahan" required name="kel_pindah" id="kel_pindah" <?php echo $id ? 'value="'.$data->kel_pindah.'"' : '' ?>>
                <input type="text" class="small" placeholder="Kecamatan" required name="kec_pindah" id="kec_pindah" <?php echo $id ? 'value="'.$data->kec_pindah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="kota_pindah">Kota/Propinsi</label>
            <div class="control-input">
                <input type="text" class="small" placeholder="Kabupaten/Kota" required name="kota_pindah" id="kota_pindah" <?php echo $id ? 'value="'.$data->kota_pindah.'"' : '' ?>>
                <input type="text" class="small" placeholder="Propinsi" required name="prov_pindah" id="prov_pindah" <?php echo $id ? 'value="'.$data->prov_pindah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="klasifikasi">Klasifikasi Pindah</label>
            <div class="control-input">
                <input type="text" required name="klasifikasi" id="klasifikasi" <?php echo $id ? 'value="'.$data->klasifikasi.'"' : '' ?> autofocus>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="jenis">Jenis Kepindahan</label>
            <div class="control-input">
                <input type="text" required name="jenis" id="jenis" <?php echo $id ? 'value="'.$data->jenis.'"' : '' ?> autofocus>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="kk_tak_pindah">Status KK yg. tdk. Pindah</label>
            <div class="control-input">
                <input type="text" required name="kk_tak_pindah" id="kk_tak_pindah" <?php echo $id ? 'value="'.$data->kk_tak_pindah.'"' : '' ?> autofocus>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="kk_pindah">Status KK yg. Pindah</label>
            <div class="control-input">
                <input type="text" required name="kk_pindah" id="kk_pindah" <?php echo $id ? 'value="'.$data->kk_pindah.'"' : '' ?> autofocus>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="tgl_pindah">Tanggal Kepindahan</label>
            <div class="control-input">
                <input type="text" class="jqui-datepicker" required name="tgl_pindah" id="tgl_pindah" <?php echo $id ? 'value="'.$data->tgl_pindah.'"' : '' ?> autofocus>
            </div>
        </div>
    </div>

    <div id="tab-keluarga">
        <div class="control-group">
            <label class="label" for="juml_keluarga">Jumlah Anggota Keluarga</label>
            <div class="control-input">
                <input type="text" required name="juml_keluarga" id="juml_keluarga" <?php echo $id ? 'value="'.$data->juml_keluarga.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="data_anggota">Data Anggota Keluarga</label>
            <div class="control-input">
            <?php $shdk = Warga::$prop['status_kel']; ?>
                <span class="hidden" id="status-kel-api"><?php echo json_encode($shdk) ?></span>
            <?php if (!$id): ?>
                <button type="button" id="add-btn" class="btn">Tambah Data</button>
            <?php endif; ?>
                <table id="t-keluarga" class="data">
                    <thead>
                        <tr>
                            <th style="width:15%">NIK</th>
                            <th style="width:30%">Nama Lengkap</th>
                            <th style="width:20%">S.H.D.K</th>
                            <th class="action">Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if ($id && !empty($data->keluarga)): foreach (unserialize($data->keluarga) as $keluarga): ?>
                        <tr>
                            <th style="width:15%"><?php echo isset($keluarga['keluarga_nik'])  ? $keluarga['keluarga_nik'] : '-' ?></th>
                            <th style="width:30%"><?php echo isset($keluarga['keluarga_nama']) ? $keluarga['keluarga_nama'] : '-' ?></th>
                            <th style="width:20%"><?php echo isset($keluarga['keluarga_shdk']) ? $shdk[$keluarga['keluarga_shdk']] : '-' ?></th>
                            <th class="action">-</th>
                        </tr>
                    <?php endforeach; else: ?>
                        <tr class="empty acenter"><td colspan="5">Belum ada data.</td></tr>
                    <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
<script>
$(function () {
    var emptyTmpl = '<tr class="empty acenter"><td colspan="5">Belum ada data.</td></tr>'
    $('#t-keluarga').tableExp(emptyTmpl, function () {
        var rowTmpl = [
            '<tr class="kel-row">',
            '<td><input type="text" name="keluarga_nik[]" placeholder="NIK" data-autocomplete="nik"></td>',
            '<td><input type="text" name="keluarga_nama[]" placeholder="Nama Lengkap"></td>',
            '<td><select name="keluarga_shdk[]">',
                '<option>S.H.D.K</option>'
        ]

        $.each(JSON.parse($('#status-kel-api').html()), function (key, val) {
            rowTmpl.push(['<option value="' + key + '">' + val + '</option>'])
        })

        rowTmpl.push([
            '<select></td>',
            '<td class="action"><button type="button" class="btn remove-row-btn">Hapus</button></td>',
            '</tr>'
        ])

        return rowTmpl
    })
})
</script>
