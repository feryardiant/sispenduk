<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT;
include dirname(__FILE__).'/../incl/form-skrw'.EXT;
include dirname(__FILE__).'/../incl/form-ttd'.EXT; ?>

<div class="control-group">
    <label class="label" for="bukti_ktp">Surat Bukti Diri</label>
    <div class="control-input">
        <input type="text" class="small" placeholder="KTP" required name="bukti_ktp" id="bukti_ktp" value="<?php echo $id ? $data->bukti_ktp : $warga->nik ?>">
        <input type="text" class="small" placeholder="KK" required name="bukti_kk" id="bukti_kk" value="<?php echo $id ? $data->bukti_kk : $warga->no_kk ?>">
    </div>
</div>

<fieldset>
    <legend>Detil Acara</legend>
    <div class="control-group">
        <label class="label" for="maksud_acara">Maksud Keramaian</label>
        <div class="control-input">
            <input type="text" required name="maksud_acara" id="maksud_acara" <?php echo $id ? 'value="'.$data->maksud_acara.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="tgl_acara">Tanggal &amp; Waktu Acara</label>
        <div class="control-input">
            <input type="text" required class="small jqui-datepicker" placeholder="Tanggal Penyelenggaraan" name="tgl_acara" id="tgl_acara" <?php echo $id ? 'value="'.$data->tgl_acara.'"' : '' ?>>
            <input type="text" required class="small" placeholder="Waktu Penyelenggaraan" name="waktu_acara" id="waktu_acara" <?php echo $id ? 'value="'.$data->waktu_acara.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="jenis_acara">Jenis Acara</label>
        <div class="control-input">
            <input type="text" required name="jenis_acara" id="jenis_acara" <?php echo $id ? 'value="'.$data->jenis_acara.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="juml_undangan">Jumlah Undangan</label>
        <div class="control-input">
            <input type="number" required name="juml_undangan" id="juml_undangan" <?php echo $id ? 'value="'.$data->juml_undangan.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="tmpt_acara">Tempat Penyelenggaraan</label>
        <div class="control-input">
            <input type="text" required name="tmpt_acara" id="tmpt_acara" <?php echo $id ? 'value="'.$data->tmpt_acara.'"' : '' ?>>
        </div>
    </div>
</fieldset>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
