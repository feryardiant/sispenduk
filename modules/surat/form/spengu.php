<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT;
include dirname(__FILE__).'/../incl/form-masaberlaku'.EXT;
include dirname(__FILE__).'/../incl/form-ttd'.EXT; ?>

<div class="control-group">
    <label class="label" for="tujuan">Keperluan</label>
    <div class="control-input">
        <input type="text" required name="tujuan" id="tujuan" <?php echo $id ? 'value="'.$data->tujuan.'"' : '' ?>>
    </div>
</div>

<div class="control-group">
    <label class="label" for="bukti_ktp">Surat Bukti Diri</label>
    <div class="control-input">
        <input type="text" class="small" placeholder="KTP" required name="bukti_ktp" id="bukti_ktp" value="<?php echo $id ? $data->bukti_ktp : $warga->nik ?>" readonly>
        <input type="text" class="small" placeholder="KK" required name="bukti_kk" id="bukti_kk" value="<?php echo $id ? $data->bukti_kk : $warga->no_kk ?>" readonly>
    </div>
</div>

<div class="control-group">
    <label class="label" for="ket_lain">Keterangan Lain</label>
    <div class="control-input">
        <input type="text" name="ket_lain" id="ket_lain" <?php echo $id ? 'value="'.$data->ket_lain.'"' : '' ?>>
    </div>
</div>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
