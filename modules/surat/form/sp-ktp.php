<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT; ?>

<div class="control-group">
    <label class="label" for="nik">NIK</label>
    <div class="control-input">
        <input type="text" required name="nik" id="nik" value="<?php echo $id ? $data->nik : $warga->nik ?>" autofocus>
    </div>
</div>

<div class="control-group">
    <label class="label" for="nama">Nama Lengkap</label>
    <div class="control-input">
        <input type="text" required name="nama" id="nama" value="<?php echo $id ? $data->nama : $warga->nama ?>">
    </div>
</div>

<div class="control-group">
    <label class="label" for="no_kk">Nomor KK</label>
    <div class="control-input">
        <input type="text" required name="no_kk" id="no_kk" value="<?php echo $id ? $data->no_kk : $warga->no_kk ?>">
    </div>
</div>

<div class="control-group">
    <label class="label" for="alamat">Alamat</label>
    <div class="control-input">
        <input type="text" required name="alamat" id="alamat" value="<?php echo $id ? $data->alamat : $warga->alamat ?>">
    </div>
</div>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
