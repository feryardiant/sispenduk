<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT;
include dirname(__FILE__).'/../incl/form-ttd'.EXT; ?>

<div class="jqui-tabs">
    <ul>
        <li><a href="#tab-pemohon">Biodata Pemohon</a></li>
        <li><a href="#tab-keluarga">Anggota Keluarga</a></li>
    </ul>

    <div id="tab-pemohon">
        <div class="control-group">
            <label class="label" for="nik">NIK</label>
            <div class="control-input">
                <input type="text" required name="nik" id="nik" value="<?php echo $id ? $data->nik : $warga->nik ?>" autofocus>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="nama">Nama Lengkap</label>
            <div class="control-input">
                <input type="text" required name="nama" id="nama" value="<?php echo $id ? $data->nama : $warga->nama ?>">
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="kk_lama">No. KK Semula</label>
            <div class="control-input">
                <input type="text" required name="no_kk" id="no_kk" value="<?php echo $id ? $data->no_kk : $warga->no_kk ?>">
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="alamat">Alamat</label>
            <div class="control-input">
                <input type="text" required name="alamat" id="alamat" value="<?php echo $id ? $data->alamat : $warga->alamat ?>">
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="rt">RT/RW/Kode Pos</label>
            <div class="control-input">
                <input type="text" class="mini" placeholder="Rt" required name="rt" id="rt" value="<?php echo $id ? $data->rt : $warga->rt ?>">
                <input type="text" class="mini" placeholder="Rw" required name="rw" id="rw" value="<?php echo $id ? $data->rw : $warga->rw ?>">
                <input type="text" class="small" placeholder="Kode Pos" required name="pos" id="pos" value="<?php echo $id ? $data->pos : $warga->pos ?>">
            </div>
        </div>
    </div>

    <div id="tab-keluarga">
        <div class="control-group">
            <label class="label" for="juml_keluarga">Jumlah Anggota Keluarga</label>
            <div class="control-input">
                <input type="text" required name="juml_keluarga" id="juml_keluarga" <?php echo $id ? 'value="'.$data->juml_keluarga.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="data_anggota">Data Anggota Keluarga</label>
            <div class="control-input">
            <?php $shdk = Warga::$prop['status_kel']; ?>
                <span class="hidden" id="status-kel-api"><?php echo json_encode($shdk) ?></span>
            <?php if (!$id): ?>
                <button type="button" id="add-btn" class="btn">Tambah Data</button>
            <?php endif; ?>
                <table id="t-keluarga" class="data">
                    <thead>
                        <tr>
                            <th style="width:15%">NIK</th>
                            <th style="width:30%">Nama Lengkap</th>
                            <th style="width:20%">Tgl. Akhir KTP</th>
                            <th style="width:20%">S.H.D.K</th>
                            <th class="action">Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if ($id && !empty($data->keluarga)): foreach (unserialize($data->keluarga) as $keluarga): ?>
                        <tr>
                            <th style="width:15%"><?php echo isset($keluarga['keluarga_nik'])     ? $keluarga['keluarga_nik'] : '-' ?></th>
                            <th style="width:30%"><?php echo isset($keluarga['keluarga_nama'])    ? $keluarga['keluarga_nama'] : '-' ?></th>
                            <th style="width:20%"><?php echo isset($keluarga['keluarga_tgl_ktp']) ? $keluarga['keluarga_tgl_ktp'] : '-' ?></th>
                            <th style="width:20%"><?php echo isset($keluarga['keluarga_shdk'])    ? $shdk[$keluarga['keluarga_shdk']] : '-' ?></th>
                            <th class="action">-</th>
                        </tr>
                    <?php endforeach; else: ?>
                        <tr class="empty acenter"><td colspan="5">Belum ada data.</td></tr>
                    <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
<script>
$(function () {
    var emptyTmpl = '<tr class="empty acenter"><td colspan="5">Belum ada data.</td></tr>'
    $('#t-keluarga').tableExp(emptyTmpl, function () {
        var rowTmpl = [
            '<tr class="kel-row">',
            '<td><input type="text" name="keluarga_nik[]" placeholder="NIK" data-autocomplete="nik"></td>',
            '<td><input type="text" name="keluarga_nama[]" placeholder="Nama Lengkap"></td>',
            '<td><input type="text" class="jqui-datepicker" name="keluarga_tgl_ktp[]" placeholder="Tgl. Akhir KTP"></td>',
            '<td><select name="keluarga_shdk[]">',
                '<option>S.H.D.K</option>'
        ]

        $.each(JSON.parse($('#status-kel-api').html()), function (key, val) {
            rowTmpl.push(['<option value="' + key + '">' + val + '</option>'])
        })

        rowTmpl.push([
            '<select></td>',
            '<td class="action"><button type="button" class="btn remove-row-btn">Hapus</button></td>',
            '</tr>'
        ])

        return rowTmpl
    })
})
</script>
