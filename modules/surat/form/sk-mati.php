<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT;
include dirname(__FILE__).'/../incl/form-ttd'.EXT; ?>

<div class="control-group">
    <label class="label" for="hub">Hubungan dengan jenazah</label>
    <div class="control-input">
        <input type="text" required name="hub_pemohon" id="hub_hub_pemohon" <?php echo $id ? 'value="'.$data->hub_pemohon.'"' : '' ?>>
    </div>
</div>

<?php $tabs = array('ibu', 'ayah', 'saksi_1', 'saksi_2') ?>
<div class="jqui-tabs">
    <ul>
        <li><a href="#tab-jenazah">Data Jenazah</a></li>
    <?php foreach ($tabs as $rel): ?>
        <?php $relLabel = $rel == 'ibu' ? $rel.' Kandung' : str_replace('_', ' ', $rel); ?>
        <li><a href="#tab-<?php echo $rel ?>"><?php echo ucfirst($relLabel) ?></a></li>
    <?php endforeach ?>
    </ul>

    <div id="tab-jenazah">
        <div class="control-group">
            <label class="label" for="nik_jenazah">NIK</label>
            <div class="control-input">
                <input type="text" required name="nik_jenazah" id="nik_jenazah" <?php echo $id ? 'value="'.$data->nik_jenazah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="nama_jenazah">Nama Lengkap</label>
            <div class="control-input">
                <input type="text" required name="nama_jenazah" id="nama_jenazah" <?php echo $id ? 'value="'.$data->nama_jenazah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="jns_kel_jenazah">Jenis Kelamin</label>
            <div class="control-input">
            <?php foreach (Warga::$prop['jns_kel'] as $val => $jns_kel_jenazah) : ?>
                <input type="radio" name="jns_kel_jenazah" id="jns_kel_jenazah-<?php echo $val ?>" value="<?php echo $val ?>" <?php echo $id && $data->jns_kel_jenazah == $val ? 'checked' : '' ?>>
                <label for="jns_kel_jenazah-<?php echo $val ?>"><?php echo $jns_kel_jenazah ?></label>
            <?php endforeach ?>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="tmpt_lahir_jenazah">Tempat &amp; Tanggal Lahir</label>
            <div class="control-input">
                <input type="text" placeholder="Tempat Lahir" class="small" required name="tmpt_lahir_jenazah" id="tmpt_lahir_jenazah" <?php echo $id ? 'value="'.$data->tmpt_lahir_jenazah.'"' : '' ?>>
                <input type="text" placeholder="Tanggal Lahir" class="small jqui-datepicker" required name="tgl_lahir_jenazah" id="tgl_lahir_jenazah" <?php echo $id ? 'value="'.$data->tgl_lahir_jenazah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="anak_ke_jenazah">Anak Ke-</label>
            <div class="control-input">
                <input type="number" required name="anak_ke_jenazah" id="anak_ke_jenazah" <?php echo $id ? 'value="'.$data->anak_ke_jenazah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="tgl_mati_jenazah">Tanggal &amp; Jam Kematian</label>
            <div class="control-input">
                <input type="text" placeholder="Tanggal Kematian" class="small jqui-datepicker" required name="tgl_mati_jenazah" id="tgl_mati_jenazah" <?php echo $id ? 'value="'.$data->tgl_mati_jenazah.'"' : '' ?>>
                <input type="text" placeholder="Jam Kematian" class="small" required name="jam_mati_jenazah" id="jam_mati_jenazah" <?php echo $id ? 'value="'.$data->jam_mati_jenazah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="sebab_mati_jenazah">Sebab Kematian</label>
            <div class="control-input">
                <input type="text" required name="sebab_mati_jenazah" id="sebab_mati_jenazah" <?php echo $id ? 'value="'.$data->sebab_mati_jenazah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="tempat_mati_jenazah">Tempat Kematian</label>
            <div class="control-input">
                <input type="text" required name="tempat_mati_jenazah" id="tempat_mati_jenazah" <?php echo $id ? 'value="'.$data->tempat_mati_jenazah.'"' : '' ?>>
            </div>
        </div>

        <div class="control-group">
            <label class="label" for="penerang_jenazah">Yang Menerangkan</label>
            <div class="control-input">
                <input type="text" required name="penerang_jenazah" id="penerang_jenazah" <?php echo $id ? 'value="'.$data->penerang_jenazah.'"' : '' ?>>
            </div>
        </div>
    </div>

    <?php include dirname(__FILE__).'/../incl/form-lahirmati'.EXT ?>
</div>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
