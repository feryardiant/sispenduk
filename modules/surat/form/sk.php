<?php defined('ABSPATH') or die ('Not allowed!');
include dirname(__FILE__).'/../incl/form-nomor'.EXT;
include dirname(__FILE__).'/../incl/form-skrw'.EXT;
include dirname(__FILE__).'/../incl/form-ttd'.EXT; ?>

<div class="control-group">
    <label class="label" for="tujuan">Keperluan</label>
    <div class="control-input">
        <input type="text" required name="tujuan" id="tujuan" <?php echo $id ? 'value="'.$data->tujuan.'"' : '' ?>>
    </div>
</div>

<div class="control-group">
    <label class="label" for="bukti_ktp">Surat Bukti Diri</label>
    <div class="control-input">
        <input type="text" class="small" placeholder="KTP" required name="bukti_ktp" id="bukti_ktp" value="<?php echo $id ? $data->bukti_ktp : $warga->nik ?>">
        <input type="text" class="small" placeholder="KK" required name="bukti_kk" id="bukti_kk" value="<?php echo $id ? $data->bukti_kk : $warga->no_kk ?>">
    </div>
</div>

<?php if ($sub2 == 'sk-usaha'): ?>
<div class="control-group">
    <label class="label" for="jns_usaha">Jenis Usaha</label>
    <div class="control-input">
        <input type="text" required name="jns_usaha" id="jns_usaha" <?php echo $id ? 'value="'.$data->jns_usaha.'"' : '' ?>>
    </div>
</div>
<?php elseif ($sub2 == 'sk-domusaha'): ?>
<fieldset>
    <legend>Data Perusahaan</legend>
    <div class="control-group">
        <label class="label" for="nama_usaha">Nama Perusahaan</label>
        <div class="control-input">
            <input type="text" required name="nama_usaha" id="nama_usaha" <?php echo $id ? 'value="'.$data->nama_usaha.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="nama_pemilik">Nama Pemilik</label>
        <div class="control-input">
            <input type="text" required name="nama_pemilik" id="nama_pemilik" <?php echo $id ? 'value="'.$data->nama_pemilik.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="alamat_usaha">Alamat Perusahaan</label>
        <div class="control-input">
            <input type="text" required name="alamat_usaha" id="alamat_usaha" <?php echo $id ? 'value="'.$data->alamat_usaha.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="jns_usaha">Jenis Usaha</label>
        <div class="control-input">
            <input type="text" required name="jns_usaha" id="jns_usaha" <?php echo $id ? 'value="'.$data->jns_usaha.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="status_usaha">Status Perusahaan</label>
        <div class="control-input">
            <input type="text" required name="status_usaha" id="status_usaha" <?php echo $id ? 'value="'.$data->status_usaha.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="juml_karyawan">Jumlah Karyawan</label>
        <div class="control-input">
            <input type="number" required name="juml_karyawan" id="juml_karyawan" <?php echo $id ? 'value="'.$data->juml_karyawan.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="luas_usaha">Luas Tempat Usaha</label>
        <div class="control-input">
            <input type="text" required name="luas_usaha" id="luas_usaha" <?php echo $id ? 'value="'.$data->luas_usaha.'"' : '' ?>>
        </div>
    </div>

    <div class="control-group">
        <label class="label" for="jam_kerja">Waktu Kerja</label>
        <div class="control-input">
            <input type="text" required name="jam_kerja" id="jam_kerja" <?php echo $id ? 'value="'.$data->jam_kerja.'"' : '' ?>>
        </div>
    </div>
</fieldset>
<?php endif ?>

<?php include dirname(__FILE__).'/../incl/form-action'.EXT ?>
