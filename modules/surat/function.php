<?php defined('ABSPATH') or die ('Not allowed!');

class Surat extends Module
{
    public static
        $subs,
        $status = array('proses', 'ditolak', 'diterima', 'dihapus');

    public function __construct()
    {
        parent::__construct();
        self::$subs = require 'subs.php';
        $this->menu->add($this, array(
            'caps'  => array(1, 2, 3, 4, 5),
            'title' => 'Surat Dinas',
            'subs'  => self::$subs,
        ));

        if (App::uriSegment(1) == 'surat' && get('p') == 'pdf') {
            $this->getPdf();
            // exit();
        }
    }

    public function getNo()
    {
        $jns = App::uriSegment(3);
        if ($query = static::$db->query("SELECT no_surat FROM tbl_surat WHERE jns_surat='%s'", $jns)) {
            $nomor = 1;
            if (($count = $query->count()) > 0) {
                $nomor = $count + 1;
            }
        }

        return $jns.'-'.substr('000'.$nomor, -3);
    }

    public function tiles()
    {
        $tile = '';
        $subs = self::$subs;

        if ($sub = App::uriSegment(2)) {
            $tmp = $subs;
            unset($subs);
            $subs[$sub] = $tmp[$sub];
        }

        foreach ($subs as $link => $prop) {
            $page = $this->user->current('level') == 5 ? '?p=form' : '';
            $tile .= '<fieldset class="sub-surat">'
                  .  '<legend>'.$prop['title'].'</legend>'
                  .  '<ul class="clearfix">';

            foreach ($prop['subs'] as $sublink => $subtitle) {
                $img = '<img src="'.siteUrl('modules/surat/asset/img/file.png').'" alt="'.$subtitle.'">';
                $tile .= '<li class="fleft">'.anchor(siteUrl('surat/'.$link.'/'.$sublink.$page), $img.'<br>'.$subtitle).'</li>';
            }

            $tile .= '</ul>';
        }

        $tile .= '</fieldset>';

        return $tile;
    }

    public function fetch($val = false, $key = '')
    {
        $jns = App::uriSegment(3);
        $sql = "SELECT
                    srt.id, srt.no_surat, srt.nik_pemohon, srt.tgl_surat, srt.jns_surat, srt.tujuan,
                    srt.status_rt, srt.status_rw, srt.status_kades,
                    pnd.nik, pnd.no_kk, pnd.nama, pnd.jns_kel, pnd.alamat
                FROM tbl_surat srt
                INNER JOIN tbl_penduduk pnd ON srt.nik_pemohon = pnd.nik";

        if ($status = get('status')) {
            $sql .= ' WHERE srt.status_kades='.$status;
        }

        if ($jns = App::uriSegment(3)) {
            $sql .= " AND srt.jns_surat='{$jns}'";
        }

        if (is_string($val) || is_int($val)) {
            $key || $key = 'id';
            if ($key != 'id') {
                $val = "'{$val}'";
            }
            $sql .= ' AND srt.'.$key.'='.$val;
        } elseif (is_array($val)) {
            $i = 0;
            if (!$status) {
                $sql .= ' WHERE';
            }
            foreach ($val as $key => $value) {
                $sql .= ($i > 0 ? ' AND' : '')
                     .  ' srt.'.$key.' = '.(is_string($value) ? "'{$value}'" : $value);
                $i++;
            }
        }

        try {
            return static::$db->query($sql, escapeString($jns));
        } catch (Exception $e) {
            App::alert($e->getMessage(), 'error');
        }
    }

    public function get($id)
    {
        $return = false;
        if ($query = self::fetch($id)) {
            $surat = $query->fetchOne();

            if ($qdetil = static::$db->select('tbl_surat_detil', '', array('id_surat' => $surat->id))) {
                $detil = new stdClass();
                foreach ($qdetil->result() as $row) {
                    $detil->{$row->nama} = $row->nilai;
                }
            }

            $qpemohon = Module::warga()->fetchOne('penduduk', $surat->nik_pemohon, 'nik');
            $return   = (object) array_merge((array) $surat, (array) $detil, (array) $qpemohon);
        }

        return $return;
    }

    public function fetch_surat($val = false, $key = '')
    {
        $where = array();

        if ($val !== false) {
            $key || $key = 'id';
            $where = array($key => $val);
        }

        return static::$db->select('tbl_surat', '', $where);
    }

    public function fetch_detil($val = false, $key = '')
    {
        $where = array();

        if ($val !== false) {
            $key || $key = 'id_surat';
            $where = array($key => $val);
        }

        return static::$db->select('tbl_surat_detil', '', $where);
    }

    public function save()
    {
        $error = 0;
        $suratData = $suratDetil= $_val = array();
        $jns_surat = App::uriSegment(3);
        $mainFields = array('no_surat', 'status_rt', 'status_rw', 'status_kades', 'ttd', 'tujuan', 'nik_pemohon');

        foreach ($_POST as $field => $postVal) {
            if ($field != 'submit') {
                if (is_numeric($postVal)) {
                    $postVal = (int) $postVal;
                }

                if (in_array($field, $mainFields)) {
                    $suratData[$field] = $postVal;
                } else {
                    if (in_array($jns_surat, array('sp-kk', 'sp-pindah'))) {
                        $_addField = array('keluarga_nik', 'keluarga_nama', 'keluarga_shdk');
                        if ($jns_surat == 'sp-kk') {
                            $_addField[] = 'keluarga_tgl_ktp';
                        }
                        if (in_array($field, $_addField)) {
                            foreach ($postVal as $i => $pVal) {
                                $_val[$i][$field] = $pVal;
                            }
                            $suratDetil['keluarga'] = serialize($_val);
                        } else {
                            $suratDetil[$field] = is_array($postVal) ? serialize($postVal) : $postVal;
                        }
                    } else {
                        $suratDetil[$field] = is_array($postVal) ? serialize($postVal) : $postVal;
                    }
                }
            }
        }

        $suratData['jns_surat'] = $jns_surat;
        $suratData['tgl_surat'] = date('Y-m-d');
        $id = get('id');
        $suratTerm = $id ? array('id' => $id, 'no_surat' => $suratData['no_surat']) : array();

        if ($id) {
            unset($suratData['no_surat']);
        }
        if ($idSurat = static::$db->save('tbl_surat', $suratData, $suratTerm)) {
            foreach ($suratDetil as $dField => $dValue) {
                $detilTerm = $id ? array('id_surat' => $id, 'nama' => $dField) : array();
                $detilData = array(
                    'nilai' => $dValue ?: '-',
                );

                if (!$id) {
                    $detilData['id_surat'] = $idSurat;
                    $detilData['nama']     = $dField;
                }

                if (!static::$db->save('tbl_surat_detil', $detilData, $detilTerm)) {
                    $error += 1;
                    break;
                }
            }
        } else {
            $error += 1;
        }

        if ($error > 0) {
            return false;
        }

        return $suratDetil;
    }

    static function changeStatus($new, $by, $id) {
        $status = array(
            'tolak'   => 1,
            'setujui' => 2,
            'hapus'   => 3
        );

        if (!isset($status[$new])) {
            return false;
        }

        $id = (int) $id;
        $data = array('status_'.$by => $status[$new]);

        return static::$db->save('tbl_surat', $data, array('id' => $id));
    }

    static function child() {
        $childs = array();
        foreach (self::$subs as $sub => $child) {
            foreach ($child['subs'] as $key => $label) {
                $childs[$key] = array(
                    'url' => siteUrl('surat/'.$sub.'/'.$key),
                    'label' => $label,
                );
            }
        }

        return $childs;
    }

    private function setResource($res) {
        foreach ($res as $field => $value) {
            if ($field == 'tgl_surat') {
                $value = formatTanggal($value);
            }
            $res->$field = $value;
        }
        return $res;
    }

    public function getPdf() {
        require dirname(__FILE__).DS.'pdf'.EXT;
        $sub1 = App::uriSegment(2);
        $sub2 = App::uriSegment(3);
        $judul = isset(self::$subs[$sub1]['subs'][$sub2]) ? self::$subs[$sub1]['subs'][$sub2] : self::$subs[$sub1]['title'];

        try {
            $pdf = new SuratPdf();
            if ($id = get('id')) {
                $res = $this->setResource(self::get($id));
                // var_dump($res);
                $pdf->surat($judul, $res);
            } else {
                $pdf->lapor($judul, self::fetch());
            }

            echo $pdf->toFile();
            exit();
        } catch (Exception $e) {
            App::alert($e);
        }
    }
}
