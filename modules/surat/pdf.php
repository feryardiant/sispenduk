<?php

class SuratPdf extends Pdf
{
    private $suratObj, $_d;

    public function header() {
        if ($this->docCop) {
            $ln = 1;
            $tln = count($this->docCop);
            $this->image('storage/logo.png', 10, 8, 18);
            $this->setFont($this->conf['fontFamily'], 'B', 10);

            foreach ($this->docCop as $cop) {
                $border = $ln == $tln ? 'B' : 0;
                $height = $ln == $tln ? 6 : 5;
                $this->cell(0, $height, $cop, $border, 0, 'C');
                $this->ln();
                $ln++;
            }

            $this->ln();
        }

        $this->setFont($this->conf['fontFamily'], 'B', 14);
        $this->cell(0, 10, trim($this->docTitle), 0, 0, 'C');
        $this->ln();

        if ($this->docCop) {
            $this->setFont($this->conf['fontFamily'], '', $this->conf['fontSize']);
            $this->cell(0, 6, trim('Nomor: '.$this->suratObj->no_surat), 0, 0, 'C');
            $this->ln();
        }
    }

    public function docCop($docTitle) {
        $this->docCop = array(
            strtoupper($this->_d['kab']),
            'KECAMATAN '.strtoupper($this->_d['kec']),
            'DESA '.strtoupper($this->_d['desa']),
            'Alamat: ' . strtoupper($this->_d['alamat']),
        );

        return $this->docTitle($docTitle);
    }

    public function lapor($title, Db $resource) {
        $columns = array(
            'no_surat'     => array('width' => 32, 'data' => 'Nomor Surat', 'align' => 'C'),
            'tgl_surat'    => array('width' => 26, 'data' => 'Tanggal Surat', 'align' => 'C', '_call' => 'formatTanggal'),
            'nama'         => array('width' => 60, 'data' => 'Nama Pemohon'),
            'tujuan'       => array('width' => 56, 'data' => 'Keperluan'),
            'status_kades' => array('width' => 15, 'data' => 'Status', 'align' => 'C', '_call' => function ($_lvlStat) {
                return ucfirst(Surat::$status[$_lvlStat]);
            }),
        );

        $this->docTitle($title);
        $this->addPage();
        $this->addTable($this->parseData($resource, $columns));

        return $this;
    }

    public function surat($title, $suratObj) {
        $this->suratObj = $suratObj;
        $this->tHead = false;
        $_dinas = array('kab', 'kec', 'desa', 'alamat', 'nama_camat', 'nama_kades', 'nip_camat', 'nip_kades');
        foreach ($_dinas as $dinas) {
            $this->_d[$dinas] = App::conf('dinas.'.$dinas);
        }

        $this->docCop($title);
        $this->addPage();
        $this->setFont($this->conf['fontFamily'], '', $this->conf['fontSize']);

        $this->parseSurat($suratObj->jns_surat);
        $this->ln();

        return $this;
    }

    private function parseSurat($jns_surat) {
        $this->ln();
        $lines = $this->_template();

        foreach ($lines as $line) {
            if (!is_array($line)) {
                $this->multiCell(0, 5, $line);
            } else {
                $this->addTable($line, 0);
            }

            $this->ln();
        }
    }

    private function _template() {
        $jnsTtd = '';
        $suratObj =& $this->suratObj;
        $biodata = array(
            'Nama'                   => $suratObj->nama,
            'Jenis Kelamin'          => $suratObj->jns_kel,
            'Bin / Binti'            => $suratObj->nama_ayah.' / '.$suratObj->nama_ibu,
            'Tempat / Tanggal Lahir' => $suratObj->tmpt_lahir.' / '.$suratObj->tgl_lahir,
            'Warga Negara'           => $suratObj->kwn,
            'Agama'                  => $suratObj->agama,
            'No. KTP / NIK'          => $suratObj->nik_pemohon,
            'Pekerjaan'              => $suratObj->pekerjaan,
            'Alamat'                 => $suratObj->alamat,
        );

        $lines = array(
            'Kepala Desa '.$this->_d['desa'].', Kecamatan '.$this->_d['kec'].', Kabupaten PEKALONGAN, dengan ini menerangkan bahwa :',
        );

        $lines[] = $this->_asdasd($biodata);
        if (in_array($suratObj->jns_surat, array('spengu', 'speru', 'spemu'))) {
            $lines[] = 'Demikian untuk menjadikan maklum bagi yang berkepentingan.';
        } elseif ($suratObj->jns_surat == 'spengpol') {
            $jnsTtd = 'camat';
            $lines[] = 'Berdasarkan Surat Keterangan dari Ketua Rukun Warga 1 Nomor Tanggal dan menurut pengakuan yang bersangkutan sampai saat ini belum pernah tersangkut YUSTISI / urusan kepolisian.';
            $lines[] = 'Surat Keterangan ini diperlukan untuk : '.$suratObj->tujuan;
            $lines[] = 'Demikian Surat Keterangan ini kami buat atas permintaan yang bersangkutan, agar yang berkepentingan mengetahui dan maklum.';
        } elseif ($suratObj->jns_surat == 'spengho') {
            $lines[] = 'Berdasarkan Surat Keterangan dari Ketua Rukun Warga 1 Nomor Tanggal , maka dengan ini menerangkan data permohonan yang bersangkutan dapat dilaksanakan dengan ketentuan sebagai berikut :';
            $lines[] = 'Demikian Surat Keterangan ini kami buat atas permintaan yang bersangkutan, agar yang berkepentingan mengetahui dan maklum.';
        } elseif (in_array($suratObj->jns_surat, array('sp-ktp', 'sp-kk', 'sp-pindah'))) {
            $lines[] = 'Berdasarkan Surat Keterangan dari Ketua Rukun Warga 1 Nomor Tanggal , maka dengan ini menerangkan data permohonan yang bersangkutan dapat dilaksanakan dengan ketentuan sebagai berikut :';
            $lines[] = 'Demikian Surat Keterangan ini kami buat atas permintaan yang bersangkutan, agar yang berkepentingan mengetahui dan maklum.';
        } elseif ($suratObj->jns_surat == 'sk-domtinggal') {
            $jnsTtd = 'camat';
            $lines[] = 'Berdasarkan Surat Keterangan dari Ketua Rukun Warga 1 Nomor Tanggal , bahwa yang bersangkutan betul warga Desa '.$this->_d['desa'].' Kecamatan '.$this->_d['kec'].' Kabupaten PEKALONGAN yang beralamat pada alamat tersebut di atas.';
            $lines[] = 'Surat Keterangan ini diperlukan untuk : '.$suratObj->tujuan;
            $lines[] = 'Demikian Surat Keterangan ini kami buat atas permintaan yang bersangkutan, agar yang berkepentingan mengetahui dan maklum.';
        } elseif ($suratObj->jns_surat == 'sk-domusaha') {
            $dataUsaha = array(
                'Nama Perusahaan'   => $suratObj->nama,
                'Nama Pemilik'      => $suratObj->jns_kel,
                'Alamat Perusahaan' => $suratObj->nama_ayah,
                'Jenis Usaha'       => $suratObj->tmpt_lahir,
                'Status Perusahaan' => $suratObj->kwn,
                'Jumlah Karyawan'   => $suratObj->agama,
                'Luas Tempat Usaha' => $suratObj->nik,
                'Waktu Usaha'       => $suratObj->pekerjaan,
            );
            $lines[] = 'Berdasarkan Surat Pernyataan Tidak Keberatan / Ijin Tetangga yang diketahui Ketua RT. 1, dan Ketua RW. 1 Nomor 1 Tanggal 1, bahwa yang bersangkutan benar telah membuka Usaha sebagai berikut :';
            $lines[] = $this->_asdasd($dataUsaha);
            $lines[] = 'Demikian Surat Keterangan Domisili Usaha ini dibuat untuk keperluan : '.$suratObj->tujuan;
            $lines[] = 'Surat ini berlaku 3 (tiga) bulan setelah dikeluarkan, bukan merupakan surat ijin, dan tidak diperkenankan untuk melakukan usaha sebelum mendapat ijin resmi dari instansi terkait.';
        } elseif ($suratObj->jns_surat == 'sk-takmampu') {
            $jnsTtd = 'camat';
            $lines[] = 'Berdasarkan Surat Keterangan dari Ketua Rukun Warga 1 Nomor Tanggal , bahwa yang bersangkutan betul warga Desa '.$this->_d['desa'].' Kecamatan '.$this->_d['kec'].', dan menurut pengakuan yang bersangkutan keadaan ekonominya TIDAK MAMPU.';
            $lines[] = 'Surat Keterangan ini diperlukan untuk : '.$suratObj->tujuan;
            $lines[] = 'Demikian Surat Keterangan ini kami buat atas permintaan yang bersangkutan dan dapat dipergunakan sebagaimana mestinya.';
        } elseif ($suratObj->jns_surat == 'sk-usaha') {
            $jnsTtd = 'camat';
            $lines[] = 'Berdasarkan Surat Keterangan dari Ketua Rukun Warga 1 Nomor Tanggal , bahwa yang bersangkutan betul warga Desa '.$this->_d['desa'].' Kecamatan '.$this->_d['kec'].', dan menurut pengakuan yang bersangkutan mempunyai usaha xxvc.';
            $lines[] = 'Surat Keterangan ini diperlukan untuk : '.$suratObj->tujuan;
            $lines[] = 'Demikian Surat Keterangan ini kami buat atas permintaan yang bersangkutan dan dapat dipergunakan sebagaimana mestinya.';
        } elseif (in_array($suratObj->jns_surat, array('sk-lahir', 'sk-mati'))) {
            $_owo = ($suratObj->jns_surat == 'sk-lahir') ? 'bayi' : 'jenazah';
            foreach (array($_owo, 'ibu', 'ayah', 'saksi_1', 'saksi_2') as $tab) {
                $data[$tab] = array(
                    'Nama Lengkap' => $suratObj->{'nama_'.$tab},
                );
                if ($tab != 'bayi') {
                    $data[$tab]['NIK'] = $suratObj->{'nik_'.$tab};
                }
                if ($tab == 'bayi') {
                    $data[$tab]['Jenis Kelamin']          = $suratObj->{'jns_kel_'.$tab};
                    $data[$tab]['Tempat dilahirkan']      = $suratObj->{'tmpt_lahir_'.$tab};
                    $data[$tab]['Hari dan Tanggal Lahir'] = $suratObj->{'hari_lahir_'.$tab}.', '.formatTanggal($suratObj->{'tgl_lahir_'.$tab});
                    $data[$tab]['Pukul']                  = $suratObj->{'waktu_lahir_'.$tab};
                    $data[$tab]['Jenis Kelahiran']        = $suratObj->{'jns_lahir_'.$tab};
                    $data[$tab]['Kelahiran ke']           = $suratObj->{'urutan_'.$tab};
                    $data[$tab]['Penolong Kelahiran']     = $suratObj->{'asisten_lahir'};
                    $data[$tab]['Berat Bayi']             = $suratObj->{'berat_'.$tab}.' Kg';
                    $data[$tab]['Panjang Bayi']           = $suratObj->{'panjang_'.$tab}.' Cm';
                } elseif ($tab == 'jenazah') {
                    $data[$tab]['Tempat dan Tanggal Lahir'] = $suratObj->{'tmpt_lahir_'.$tab}.', '.formatTanggal($suratObj->{'tgl_lahir_'.$tab});
                    $data[$tab]['Anak ke']                  = $suratObj->{'anak_ke_'.$tab};
                    $data[$tab]['Tanggal Kematian']         = $suratObj->{'tgl_mati_'.$tab};
                    $data[$tab]['Sebab Kematian']           = $suratObj->{'sebab_mati_'.$tab};
                    $data[$tab]['Tempat Kematian']          = $suratObj->{'tempat_mati_'.$tab};
                    $data[$tab]['Yang Menerangkan']         = $suratObj->{'penerang_'.$tab};
                } else {
                    $data[$tab]['Tanggal Lahir']   = formatTanggal($suratObj->{'tgl_lahir_'.$tab});
                    $data[$tab]['Pekerjaan']       = $suratObj->{'pekerjaan_'.$tab};
                    $data[$tab]['Alamat']          = $suratObj->{'alamat_'.$tab};
                }
                if ($tab == 'ibu' || $tab == 'ayah') {
                    $data[$tab]['Kewarganegaraan'] = 'WNI';
                }
                if ($tab == 'ibu' && $suratObj->jns_surat == 'sk-lahir') {
                    $data[$tab]['Kebangsaan'] = $suratObj->{'kebangsaan_'.$tab};
                    $data[$tab]['Tgl. Pencatatan Nikah'] = formatTanggal($suratObj->{'tgl_nikah_'.$tab});
                }
                $lines[] = 'Data '.ucfirst(str_replace('_', ' ', $tab));
                $lines[] = $this->_asdasd($data[$tab]);
            }

            $lines[] = 'Demikian Surat Keterangan ini kami buat atas permintaan yang bersangkutan dan dapat dipergunakan sebagaimana mestinya.';
        } else {
            $lines[] = 'Demikian untuk menjadikan maklum bagi yang berkepentingan.';
        }
        $lines[] = $this->_ttd($jnsTtd);

        return $lines;
    }

    private function _asdasd($asdasd) {
        $lines = array();
        $a = $this->contentWidth - 55;

        foreach ($asdasd as $field => $label) {
            $lines[] = array(
                array('width' => 50, 'data' => $field),
                array('width' => 5,  'data' => ':'),
                array('width' => $a, 'data' => $label),
            );
        }

        return $lines;
    }

    private function _ttd($jnsTtd = '') {
        if (in_array($this->suratObj->jns_surat, array('sk-lahir', 'sk-mati'))) {
            $ttd = array(
                ''      => $this->_d['desa'].', '.$this->suratObj->tgl_surat,
                1       => '',
                ' '     => 'Kepala Desa '.$this->_d['desa'],
                2       => '',
                3       => '',
                '   '   => $this->_d['nama_kades'],
                '    '  => 'NIP. '.$this->_d['nip_kades'],
            );
        } elseif ($jnsTtd == 'camat') {
            $ttd = array(
                'No. Reg : __________________' => $this->_d['desa'].', '.$this->suratObj->tgl_surat,
                'Tanggal : __________________' => '',
                1                              => '',
                'Mengetahui,'                  => '',
                'Camat '.$this->_d['kec']      => 'Kepala Desa '.$this->_d['desa'],
                2                              => '',
                3                              => '',
                $this->_d['nama_camat']        => $this->_d['nama_kades'],
                'NIP. '.$this->_d['nip_camat'] => 'NIP. '.$this->_d['nip_kades'],
            );
        } else {
            $ttd = array(
                ''                    => $this->_d['desa'].', '.$this->suratObj->tgl_surat,
                1                     => '',
                'Pemohon,'            => 'Kepala Desa '.$this->_d['desa'],
                2                     => '',
                3                     => '',
                $this->suratObj->nama => $this->_d['nama_kades'],
                ' '                   => 'NIP. '.$this->_d['nip_kades'],
            );

        }

        $arrs = array();
        $tWidth = $this->contentWidth / 2;
        foreach ($ttd as $field => $label) {
            $field = is_int($field) ? '' : $field;
            $arrs[] = array(
                array('align' => 'C', 'width' => $tWidth, 'data' => $field),
                array('align' => 'C', 'width' => $tWidth, 'data' => $label),
            );
        }

        return $arrs;
    }
}
