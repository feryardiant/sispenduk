--
-- MySQL 5.5.40
-- Tue, 18 Nov 2014 06:56:29 +0000
--

DROP TABLE IF EXISTS `tbl_surat`;
CREATE TABLE `tbl_surat` (
   `id` int(11) not null auto_increment,
   `no_surat` varchar(15) not null,
   `tgl_surat` date not null,
   `jns_surat` varchar(30) not null,
   `status_rt` tinyint(1) not null default 0,
   `status_rw` tinyint(1) not null default 0,
   `status_kades` tinyint(1) not null default 0,
   `nik_pemohon` char(16) not null,
   `tujuan` varchar(100) not null,
   `ttd` varchar(20),
   PRIMARY KEY (`id`),
   UNIQUE KEY (`no_surat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- [Table `tbl_surat` is empty]

DROP TABLE IF EXISTS `tbl_surat_detil`;
CREATE TABLE `tbl_surat_detil` (
   `id` int(11) not null auto_increment,
   `id_surat` int(11) not null,
   `nama` varchar(255) not null,
   `nilai` text,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- [Table `tbl_surat_detil` is empty]
