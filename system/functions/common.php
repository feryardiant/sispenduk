<?php defined('ABSPATH') or die ('Not allowed!');

/**
 * Request Helper
 * -------------------------------------------------------------------------- */

/**
 * Mendapatkan nilai dari $_REQUEST request
 *
 * @param   string  Nama field
 * @return  string
 */
function req($key) {
    if (isset($_REQUEST[$key]))
        return escape($_REQUEST[$key]);
    return;
}

/**
 * Mendapatkan nilai dari $_GET request
 *
 * @param   string  Nama field
 * @return  string
 */
function get($key) {
    if (isset($_GET[$key]))
        return escape($_GET[$key]);
    return;
}

/**
 * Mendapatkan nilai dari $_POST request
 *
 * @param   string  Nama field
 * @return  string
 */
function post($key) {
    if (isset($_POST[$key]))
        return escape($_POST[$key]);
    return;
}

/**
 * String Helper
 * -------------------------------------------------------------------------- */

/**
 * Menyaring karakter dari $string
 *
 * @param   string  $string  String yang akan disarung
 * @return  string
 */
function escapeString($string) {
    if (!is_string($string)) {
        return;
    }

    return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
}

/**
 * Menyaring karakter dari $char
 *
 * @param   string  $char  String yang akan disarung
 * @return  string
 */
function escape($char) {
    if (is_numeric($char) || is_int($char)) {
        return (int) $char;
    } elseif (is_string($char)) {
        return htmlspecialchars($char, ENT_QUOTES, 'UTF-8');
    }
}

/**
 * Formating Helper
 * -------------------------------------------------------------------------- */

/**
 * Get formated number
 *
 * @param   double  $number   Decimal number
 * @param   string  $desimal  Decimal count
 * @param   string  $bts_des  Decimal number separator
 * @param   string  $bts_rbn  Tausans number separator
 * @return  string
 */
function formatAngka($number, $desimal = '', $bts_des = '', $bts_rbn = '') {
    $bts_des || $bts_des = ',';
    $bts_rbn || $bts_rbn = '.';
    $desimal || $desimal = 2;

    if (is_numeric($number) || is_double($number)) {
        return number_format($number, $desimal, $bts_des, $bts_rbn);
    }

    return $number;
}

/**
 * Get formated date from $fmt_date config
 *
 * @param   string  $string  String that will formated
 * @param   string  $format  Date Format (leave it empty to use default config)
 * @return  string
 */
function formatTanggal($string, $format = '') {
    $format || $format = App::conf('fmtdate');

    return date($format, strtotime($string));
}
