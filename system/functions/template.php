<?php defined('ABSPATH') or die ('Not allowed!');

/**
 * Menu
 * -------------------------------------------------------------------------- */

function siteMenu($attrs = '') {
    $app =& App::instance();
    return $app->menu->show($attrs);
}

/**
 * Templates
 * -------------------------------------------------------------------------- */

$path = App::conf('template') ?: 'base';
define('TMPLPATH', 'templates'.DS.$path.DS);

function template($layout) {
    $app =& App::instance();
    $layout_path = templateDir($layout, true);

    $app->render($layout_path);
}

function templateDir($path = '', $absolute = false) {
    $abspath = $absolute == true ? ABSPATH : '';
    return $abspath.TMPLPATH.$path;
}

function templateUri($path = '') {
    return siteUrl(TMPLPATH.$path);
}
