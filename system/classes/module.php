<?php defined('ABSPATH') or die ('Not allowed!');

class Module
{
    protected static $db;

    public function __construct($configs = array()) {
        static::$db =& Db::instance();
    }

    public static function __callStatic($module, $param = array()) {
        return App::modInstance($module);
    }

    public function __get($module) {
        $app =& App::instance();
        if (isset($app->$module)) {
            $_mod =& $app->$module;
            return $_mod;
        } elseif ($mod = App::modInstance($module)) {
            return $mod;
        }
    }
}

// EOF module.php
