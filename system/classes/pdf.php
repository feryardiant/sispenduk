<?php defined('ABSPATH') or die ('Not allowed!');

class Pdf extends Fpdf\Fpdf {
    protected
        $docTitle, $docCop, $docDate, $contentWidth,
        $tHead = true,
        $conf = array(
            'fontFamily' => 'Times',
            'fontStyle' => '',
            'fontSize' => 11,
            'tableBorder' => 1,
        ),
        $colDefault = array(
            'data'  => '',
            'width' => null,
            'align' => 'L',
            'style' => '',
            '_call' => null,
        );

    public function __construct($orientation = '', $unit = '', $size = '') {
        $orientation || $orientation = 'P';
        $unit || $unit = 'mm';
        $size || $size = 'A4';

        parent::__construct($orientation, $unit, $size);
        $this->setMargins(10, 10);
        $this->setCreator(App::conf('app.title'));

        list($pWidth, $pHeight) = $this->_getPageSize($this->curPageSize);
        $this->contentWidth = $pWidth - $this->rMargin - $this->lMargin;
    }

    public function docTitle($docTitle) {
        $this->docTitle = $docTitle;
        $this->docDate = date('d M Y, H');
        $this->setTitle($docTitle.' - '.$this->docDate);
    }

    public function toFile() {
        $file = $this->docTitle.' - '.$this->docDate.'.pdf';
        $this->output();

        if (file_exists($file)) {
            $dest = ABSPATH.'storage'.DS.$file;
            @rename($file, $dest);

            if ($handle = fopen($dest, 'r')) {
                $dest = str_replace(ABSPATH, '', $dest);
                header('Content-Type: application/x-download');
                header('Content-Disposition: attachment; filename="'.$file.'"');
                header('Cache-Control: private, max-age=0, must-revalidate');
                header('Pragma: public');
                echo fread($handle, filesize($dest));
                fclose($handle);
            }
        }
    }

    protected function parseData(Db $resource, array $columns) {
        $i = 0;
        $cell = array();
        $colCount = count($columns);
        $cellWidth = floor($this->contentWidth / $colCount);

        foreach ($columns as $field => $column) {
            if (is_string($column)) {
                $column = array('data' => $column);
            }
            $column = arraySetDefaults($column, $this->colDefault);
            $column['width'] = $column['width'] ?: $cellWidth;
            $columns[$field] = $column;
            $cell[$i][$field] = array(
                'data'  => $column['data'],
                'width' => $column['width'],
                'align' => $column['align'],
                'style' => $column['style'],
            );
        }

        $i += 1;
        if ($resource->count() > 0) {
            foreach ($resource->result() as $row) {
                foreach ($columns as $field => $column) {
                    $_val = isset($row->$field) ? $row->$field : '-';
                    $_fWidth = $this->getStringWidth($_val);
                    if ($column['_call'] !== null) {
                        $_val = call_user_func($column['_call'], $_val);
                    }
                    $cell[$i][$field] = array(
                        'data'  => $_val,
                        'width' => $column['width'],
                        'align' => $column['align'],
                        'style' => $column['style'],
                    );
                }
                $i++;
            }
        } else {
            $cell[$i][''] = array(
                'data'  => 'Kosong',
                'width' => $this->contentWidth,
                'align' => 'C',
                'style' => 'B',
            );
        }

        return $cell;
    }

    public function addTable(array $data, $border = null) {
        $i = 0;
        if ($border !== 0) {
            $border = $this->conf['tableBorder'];
        }

        foreach ($data as $row) {
            if ($this->tHead == true && $i == 0) {
                $this->setFont($this->conf['fontFamily'], 'B', $this->conf['fontSize']);
            } else {
                $this->setFont($this->conf['fontFamily'], '', $this->conf['fontSize']);
            }

            foreach ($row as $column => $cell) {
                if (is_string($cell)) {
                    $cell = array('data' => $cell);
                }
                $cell = arraySetDefaults($cell, $this->colDefault);
                $cell['align'] = ($this->tHead == true && $i == 0) ? 'C' : $cell['align'];
                $this->cell($cell['width'], 5, $cell['data'], $border, 0, $cell['align']);
            }

            $this->ln();
            $i++;
        }
    }
}
