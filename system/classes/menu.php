<?php defined('ABSPATH') or die ('Not allowed!');

class Menu
{
    public static $list = array();

    protected static $order = 0;

    public function add($name, array $configs = array()) {
        if (is_subclass_of($name, 'Module')) {
            $name = get_class($name);
            $name = strtolower($name);
        }

        $configs = arraySetDefaults($configs, array(
            'caps'  => array(),
            'title' => '',
            'order' => 1,
            'subs'  => null,
        ));

        if ($configs['order'] > 0 && !isset(self::$list[$name])) {
            $configs['order'] += self::$order;
            self::$order += 1;
        }

        self::$list[$name] = $configs;
    }

    protected function getEnabled() {
        $list = array();
        foreach (self::$list as $name => $configs) {
            if (App::enabledMod($name) || $name == 'home') {
                $list[$name] = $configs;
            }
        }

        return $list;
    }

    public function show($attr = '', $menus = array(), $parent = '', $trail = '') {
        $attr || $attr = 'class="hmenu"';
        $output = '<ul '.$attr.'>';

        if (empty($menus)) {
            $menus = $this->getEnabled();
            $totalMenu = count($menus);
            $order = array();

            foreach ($menus as $mkey => $mattr) {
                $order[$mkey] = $mattr['order'] > 0 ? $mattr['order'] : $totalMenu;
            }

            array_multisort($order, SORT_ASC, $menus);
        }

        if ($parent !== '') {
            $trail || $trail = '/';
            $parent = $parent.$trail;
        }


        foreach ($menus as $link => $menuattr) {
            if (is_string($menuattr)) {
                $menuattr = array('title' => $menuattr);
            }

            if (empty($menuattr['caps']) || in_array(Module::user()->current('level'), $menuattr['caps'])) {
                $link = $parent.strtolower($link);
                $output .= '<li '.$this->current($link).'>'
                        .  anchor($link, $menuattr['title']);

                if (!empty($menuattr['subs'])) {
                    $output .= $this->show('class="submenu"', $menuattr['subs'], $link, $trail);
                }

                $output .= '</li>';
            }
        }

        $output .= '</ul>';

        return $output;
    }

    private function current($link) {
        $baseurl = App::conf('baseurl');
        $link || $link = $baseurl;
        if (!($current = App::getUri())) {
            $current = $baseurl;
        }

        return strpos($current, $link) !== false ? 'class="active"' : '';
    }

    public static function toolbar($toolbars, $class = '') {
        $class || $class = 'page-toolbar';
        $out = '<nav class="'.$class.'">';

        foreach ($toolbars as $link => $label) {
            if (is_array($label)) {
                $out .= static::toolbar($label, 'btn-group toolbar-btn');
            } else {
                $id = strtolower($label);
                $id = str_replace(' ', '-', $id);
                $attrs = array(
                    'label' => $label,
                    'class' => 'btn toolbar-btn',
                    'id'    => $id.'-btn',
                );

                if (substr($link, -7, 7) == ':dialog') {
                    $link = str_replace(':dialog', '', $link);
                    $attrs['data-dialog'] = '';
                }

                $attrs['href'] = strpos($link, '?') === 1 ? currentUrl($link) : siteUrl($link) ;
                $out .= anchor($attrs);
            }
        }

        $out .= '</nav>';
        return $out;
    }
}

// EOF menu.php
