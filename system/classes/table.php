<?php defined('ABSPATH') or die ('Not allowed!');

class Table
{
    public
        $rowNum = false,
        $noButtons = false,
        $extraAttr = '';

    protected
        $resource,
        $columns = array(),
        $buttons = array(),
        $btn_class = array('class' => 'btn'),
        $caption = array(
            'text' => '',
            'attr' => '',
        );

    public function __construct(Db $resource, array $columns = array()) {
        $this->resource = $resource;

        if (!empty($columns)) {
            $this->setColumns($columns);
        }
    }

    public function setCaption($caption, $attrs) {
        $this->caption['text'] = $caption;
        $this->caption['attr'] = $attrs;
    }

    public function setButton($field, $link, $label, $extra = array()) {
        $this->buttons[$link] = array(
            'field' => $field,
            'link'  => $link,
            'label' => $label,
            'extra' => $extra,
        );

        return $this;
    }

    public function setColumns(array $columns) {
        foreach ($columns as $field => $column) {
            if (is_string($column)) $column = array('label' => $column);
            $column = arraySetDefaults($column, array(
                'label' => '',
                'width' => null,
                'extra' => '',
                '_link' => '',
                '_call' => null,
                '_frmt' => null,
            ));

            if ($column['extra'] != '') {
                $column['extra'] = ' '.$column['extra'];
            }

            if ($column['_link'] != '') {
                $column['label'] = anchor($column['_link'], $column['label']);
            }

            $this->columns[$field] = $column;
        }

        return $this;
    }

    public function generate(array $columns = array()) {
        $colCount = count($this->columns);
        $html = '<table class="data"'.($this->extraAttr ? ' '.$this->extraAttr : '').'>';

        if ($this->caption['text']) {
            $html .= '<caption'.($this->caption['attr'] ? ' '.$this->caption['attr'] : '').'>'.$this->caption['text'].'</caption>';
        }


        if ($this->noButtons === false && empty($this->buttons)) {
            $this->setButton('id', '?p=form&id=', 'Lihat', array('class' => 'btn btn-edit'));
            $this->setButton('id', '?p=hapus&id=', 'Hapus', array('class' => 'btn btn-hapus'));
        }

        $html .= '<thead><tr>';
        if ($this->rowNum) {
            $html .= '<th style="width:5%;">No</th>';
        }

        foreach ($this->columns as $field => $column) {
            if ($column['width'] == null) {
                $column['width'] = 90 / $colCount;
            }
            $html .= '<th style="width:'.$column['width'].'%;">'.$column['label'].'</th>';
            $this->columns[$field]['width'] = $column['width'];
        }

        if (!empty($this->buttons)) {
            $html .= '<th class="action">Pilihan</th>';
        }
        $html .= '</tr></thead><tbody>';

        if ($this->resource && ($total = $this->resource->count()) > 0) {
            $no = rowNum();
            foreach ($this->resource->result(true) as $row) {
                $html .= '<tr>';
                if ($this->rowNum) {
                    $html .= '<td class="acenter">'.$no.'</td>';
                }

                foreach ($this->columns as $field => $column) {
                    if (strpos($field, ' ') !== false) {
                        $_column = array();
                        foreach (explode(' ', $field) as $_f) {
                            $_column[] = $row->$_f;
                        }
                    } else {
                        $_column = $row->$field;
                    }

                    if ($column['_call'] !== null && is_callable($column['_call'])) {
                        if (is_array($_column)) {
                            $_column = call_user_func_array($column['_call'], $_column);
                            if (is_array($_column) && empty($column['_frmt'])) {
                                $column['_frmt'] = '';
                                foreach ($_column as $_c) {
                                    $column['_frmt'] .= ' %s';
                                }
                            }
                        } elseif (is_string($_column)) {
                            $_column = call_user_func($column['_call'], $_column);
                        }
                    }

                    if ($column['_frmt'] !== null) {
                        if (is_array($_column)) {
                            $_column = vsprintf($column['_frmt'], $_column);
                        } elseif (is_string($_column)) {
                            $_column = sprintf($column['_frmt'], $_column);
                        }
                    }

                    if (is_array($_column)) {
                        $_column = trim(implode(' ', $_column));
                    }

                    $html .= '<td'.$column['extra'].'>'.$_column.'</td>';
                }

                if (!empty($this->buttons)) {
                    $html .= '<td class="action"><div class="btn-group">';
                    foreach ($this->buttons as $link => $button) {
                        $btnExtra = arraySetDefaults($button['extra'], $this->btn_class);
                        $html .= anchor($button['link'].$row->{$button['field']}, $button['label'], $btnExtra);
                    }
                    $html .= '</div></td>';
                }

                $html .= '</tr>';
                $no++;
            }
        } else {
            $add = $this->rowNum ? 2 : 1;
            $html .= '<tr><td colspan="'.($colCount + $add).'" style="text-align:center;">Belum ada data.</td></tr>';
        }

        $html .= '</tbody></table>';

        if ($this->resource) {
            $html .= '<div class="data-info clearfix">'
                  .  '<p class="data-total">Total data: '.$total.'</p>'
                  .  '<div class="data-page">'.paginate($total).'</div>'
                  .  '</div>';
        }

        return $html;
    }
}
