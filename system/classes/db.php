<?php defined('ABSPATH') or die ('Not allowed!');

class Db
{
    private
        $_db, $_results, $_sql,
        $_num_rows = 0;

    private static $instance;

    // Konfigurasi
    protected $configs = array(
        'host' => '',
        'user' => '',
        'pass' => '',
        'name' => '',
        'port' => '',
        'pref' => '',
    );

    /**
     * Class Constructor
     *
     * @param  array  $configs  Konfigurasi
     */
    public function __construct(array $configs = array()) {
        // Menerapkan setiap konfigurasi dari $configs
        $this->configs = arraySetValues($this->configs, $configs);
        if (empty($this->configs['port'])) {
            $this->configs['port'] = ini_get('mysqli.default_port');
        }

        if (!empty($this->configs['host']) && !empty($this->configs['user'])) {
            $this->connect();
        }

        self::$instance =& $this;
    }

    public static function &instance() {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Koneksi database
     *
     * @return  void
     */
    public function connect(array $configs = array()) {
        if (!empty($configs) && empty($this->configs)) {
            new self($configs);
        }

        foreach (array('host', 'user', 'pass', 'name', 'port') as $conf) {
            $$conf = $this->configs[$conf];
        }

        try {
            // Konek ke database menggunakan mysqli driver
            if (!$this->_db = new mysqli($host, $user, $pass, $name, $port)) {
                App::error('Could not connect to database, please check your configs.php file.');
            }

            $this->_db->set_charset('utf8');
        } catch (Exception $e) {
            App::alert($e);
        }
    }

    /**
     * Eksekutor
     * ---------------------------------------------------------------------- */

    /**
     * Query database
     *
     * @param   string        $sql          SQL Query
     * @param   array|string  $replacement  Replacement
     * @return  mixed
     */
    public function query($sql, $replacement = '') {
        // Menerapkan $replacement ke pernyataan $sql
        if ($replacement) {
            if (is_array($replacement)) {
                $sql = vsprintf($sql, $replacement);
            } elseif (is_string($replacement)) {
                $sql = sprintf($sql, $replacement);
            }
        }

        try {
            // Return 'false' kalo belum ada koneksi
            if (!$this->_db) {
                $this->connect();
            }

            $this->_sql = $sql;

            // Eksekusi SQL Query
            if ($this->_db && ($this->_results = $this->_db->query($sql))) {
                if (is_bool($this->_results)) {
                    return $this->_results;
                } else {
                    $this->_num_rows = $this->_results->num_rows;
                    return $this;
                }
            } else {
                App::error($this->_db->error.'<br>'.$this->_sql);
            }
        } catch (Exception $e) {
            App::alert($e);
        }
    }

    public function count() {
        return $this->_num_rows;
    }

    protected function doLimit($limit) {
        // Mendapatkan nilai untuk pembagian jumlah data ditampilkan tiap halaman
        $hal = get('hal') ?: 1;
        // Jika $limit bernilai 0 atau 'true' maka gunakan konfigurasi $db_limit
        if ($limit === 0 || $limit === true) {
            $limit = App::conf('db.limit');
        }

        if ($this->_results) {
            $this->_results->close();
        }

        // Jika $limit bernilai 1
        $db_limit = $limit !== 1 ? ($hal * $limit - $limit).', '.$limit : $limit;
        $_num_rows = $this->_num_rows;
        $this->_sql .= ' LIMIT '.$db_limit;
        $this->query($this->_sql);
        $this->_num_rows = $_num_rows;
    }

    public function result($limit = null) {
        return $this->fetch($limit);
    }

    public function fetch($limit = null) {
        if ($limit !== null) {
            $this->doLimit($limit);
        }

        $result = array();
        if ($this->_results) {
            // Lakukan perulangan dari hasil query
            while ($row = $this->_results->fetch_object()) {
                $result[] = $row;
            }
            $this->_results->close();
        }

        return $result;
    }

    public function fetchOne() {
        $result = $this->result(1);
        return array_shift($result);
    }

    /**
     * Utama
     * ---------------------------------------------------------------------- */

    /**
     * Menampilkan data dari Database
     *
     * @param   string        $table    Nama Tabel
     * @param   string        $column   Kolom
     * @param   array         $where    Pernyataan `where` dalam array
     * @param   bool|integer  $limit    Batasan output
     * @return  mixed
     */
    public function select($table, $column = '', $where = array()) {
        $column || $column = '*';
        $where = $this->_parseWhere($where);

        try {
            return $this->query("SELECT %s FROM `%s` %s", array($column, $table,  $where));
        } catch (Exception $e) {
            App::alert($e);
        }
    }

    /**
     * Menyimpan data baru kedalam $table
     *
     * @param   string  $table  Nama Tabel
     * @param   array   $data   Data yang akan dimasukan
     * @return  bool
     */
    public function insert($table, $data = array()) {
        $column = '';
        $values = '';
        $i = 0;

        foreach($data as $col => $value) {
            if ($i > 0) {
                $column .= ", ";
                $values .= ", ";
            }

            $column .= "`$col`";

            if (is_string($value)) {
                $value = $this->escape($value);
                $values .= "'{$value}'";
            } elseif (is_numeric($value)) {
                $values .= (int) $value;
            }

            $i++;
        }

        try {
            if ($this->query("INSERT INTO `%s` (%s) VALUES (%s)", array($table, $column, $values))) {
                return $this->getInsertId();
            }

            return false;
        } catch (Exception $e) {
            App::alert($e);
        }
    }

    /**
     * Memperbarui data pada $table
     *
     * @param   string  $table  Nama Tabel
     * @param   array   $data   Data yang akan diperbarui
     * @param   array   $where  Kodisi
     * @return  bool
     */
    public function update($table, $data = array(), $where = array()) {
        $wheres = $this->_parseWhere($where);
        $data = $this->_parseArgs($data, ',');

        try {
            if ($this->query("UPDATE `%s` SET %s %s", array($table, $data, $wheres))) {
                return in_array($table, array('tbl_surat', 'tbl_surat_detil')) && isset($where['id']) ? $where['id'] : true;
            }

            return false;
        } catch (Exception $e) {
            App::alert($e);
        }
    }

    /**
     * Menambah atau memperbarui record
     *
     * @param   string  $table  Nama Table
     * @param   array   $data   Data yang akan ditambahkan atau diperarui
     * @param   array   $term   Identifikasi field, gunakan hanya untuk pembaruian
     * @return  bool
     */
    public function save($table, array $data = array(), array $term = array()) {
        if (!empty($term)) {
            $return = $this->update($table, $data, $term);
        } else {
            $return = $this->insert($table, $data);
        }

        return $return;
    }

    /**
     * Menghapus data pada $table
     *
     * @param   string  $table  Nama Tabel
     * @param   array   $where  Kondisi
     * @return  bool
     */
    public function delete($table, $where = array()) {
        $wheres = $this->_parseWhere($where);

        try {
            return $this->query("DELETE FROM `%s` %s", array($table, $wheres));
        } catch (Exception $e) {
            App::alert($e);
        }
    }

    // -------------------------------------------------------------------------

    public function import($filename) {
        $error = 0;
        $lines = file($filename);
        $query = '';

        foreach ($lines as $line) {
            if (substr($line, 0, 2) == '--' || $line == '') continue;

            $query .= trim($line).' ';

            if (substr(trim($line), -1, 1) == ';') {
                if (!$this->query($query)) {
                    $error++;
                    break;
                } else {
                    $query = '';
                }
            }
        }

        return $error == 0 ? true : false;
    }

    // -------------------------------------------------------------------------

    public function export() {}

    // -------------------------------------------------------------------------

    /**
     * Pengolahan array agar menghasilkan klausa WHERE untuk sql query
     *
     * @param   array|string  $where  Klausa untuk diolah
     * @return  string
     */
    protected function _parseWhere($where) {
        if (empty($where)) return;
        $return = 'WHERE';

        // Jika klausa merupakan array
        if (is_array($where)) {
            // foreach ($where as $field => $val) {
            //     $return .= 'WHERE';
            // }
            return $this->_parseWhere($this->_parseArgs($where, 'AND'));
        } elseif (is_string($where)) {
            return 'WHERE '.$where;
        }
    }

    /**
     * Mengolah array menjadi suatu klausa tertentu untuk digunakan dalam database
     *
     * @param   array   $args   Array untuk diolah
     * @param   string  $sep    Pembatasan tertentu
     * @return  string
     */
    protected function _parseArgs(array $args, $sep = '') {
        $i = 0;
        $attr = '';

        foreach ($args as $key => $value) {
            if (!empty($key)) {
                if (is_numeric($value) || is_int($value)) {
                    $attr .= " `{$key}`={$value}";
                } elseif (is_string($value)) {
                    $value = $this->escape($value);
                    $attr .= " `{$key}`='{$value}'";
                }

                if (count($args) > 1 && (count($args) - 1) != $i)
                    $attr .= ' '.$sep.' ';
                $i++;
            }
        }

        $attr = trim($attr);
        $return = rtrim($attr, $sep);

        return $return;
    }

    /**
     * Mendapatkan nilai primary key dari data yang baru saja di masukan (simpan)
     *
     * @return  string
     */
    function getInsertId() {
        if ($this->_db) {
            return $this->_db->insert_id;
        }
    }

    /**
     * Menyaring karakter yang tidak diinginkan agar tidak masuk ke database
     *
     * @param   string  $str  Karakter yang akan disaring
     * @return  string
     */
    function escape($str) {
        if ($this->_db) {
            return $this->_db->real_escape_string($str);
        }
    }
}

// EOF db.php
