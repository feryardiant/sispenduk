# SISPENDUK Kab. Pekalongan

DEMO: http://sispenduk.herokuapp.com/

## Requirements

+ Make sure you have activated `mod_rewrite`
+ You have [Composer](http://getcomposer.org) installed on your machine
+ Setup your `php` executable in `$PATH` environment (optional)

## Installation

+ Clone or [Download](get/master.zip) this repo and put it in your `docroot` folder,
+ Rename `configs_sample.php` to `configs.php`, and do as you wish :grin:,
+ Configure the `.htaccess` file,
+ Install composer dependencies,
+ Enjoy :grin:

**NB:**

This package also available in [packagist](https://packagist.org/). So, if you're as lazy as me, simply run:

```bash
$ cd /path/to/your/docroot/
$ composer create-project feryardiant/php-startapp myapp
```

