<?php defined('ABSPATH') or die('Not allowed!');

return array(
    // URL Aplikasi. (http://localhost/aplikasi)
    'baseurl' => '',
    // Aktifak sesi
    'session' => true,
    // Template
    'template' => 'sispenduk',
    // Tentang Aplikasi
    'app' => array(
        // Judul Aplikasi
        'title' => 'SISPENDUK Kab. Pekalongan',
        // Keterangan Aplikasi.
        'desc' => 'Sistem Informasi Pelayanan Kenendudukan',
    ),
    'dinas' => array(
        'kab' => 'PEMERINTAH KABUPATEN PEKALONGAN',
        'kec' => 'Talun',
        'desa' => 'Krompeng',
        'alamat' => 'Jl. Raya',
        'nama_camat' => 'Nama Camat',
        'nama_kades' => 'Nama Kades',
        'nip_camat' => '123456789',
        'nip_kades' => '123456789',
    ),
    'db' => array(
        // Database Host
        'host' => '',
        // Database Username
        'user' => '',
        // Database Password
        'pass' => '',
        // Database Name
        'name' => '',
        // Database Output limit
        'limit' => 10,
    ),
);
