<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <title><?php echo App::conf('app.title') ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex, nofollow">
    <!-- FAVICON -->
    <link href="<?php echo templateUri('asset/img/favicon.ico') ?>" rel="shortcut icon">
    <!-- END FAVICON -->
    <!-- CSS -->
    <link href="<?php echo templateUri('lib/css/jquery-ui.css') ?>" rel="stylesheet">
    <link href="<?php echo templateUri('asset/css/style.css') ?>" rel="stylesheet">
    <!-- END CSS -->
    <!-- JS -->
    <script src="<?php echo templateUri('lib/js/jquery.min.js') ?>"></script>
    <script src="<?php echo templateUri('lib/js/jquery-ui.min.js') ?>"></script>
    <script src="<?php echo templateUri('asset/js/script.js') ?>"></script>
    <!-- END JS -->
</head>
<body <?php bodyAttrs()?> data-siteurl="<?php echo siteUrl() ?>">
    <div class="wrapper sticky-wrap">
        <div class="sticky-head">
            <header id="site-header">
                <div id="brand">
                    <h3><?php echo App::conf('app.title') ?></h3>
                    <span><?php echo App::conf('app.desc') ?></span>
                </div>
        <?php if (App::session('auth') !== false) : ?>
            <?php echo siteMenu('id="site-nav" class="nav-menu vmenu clearfix"') ?>
            <div class="user-menu">
                <?php echo $userMod->menu() ?>
            </div>
        <?php endif ?>
            </header>
            <div id="site-contents" class="clearfix">
