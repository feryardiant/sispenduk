<?php defined('ABSPATH') or die ('Not allowed!');
template('header'); ?>
<header id="content-header" class="clearfix">
    <h3 id="page-title">
        <?php echo 'Halaman '.(!empty($do) ? ucfirst($do) : 'Beranda: Selamat datang '.$userMod->current('nama')) ?>
    </h3>
</header>
<div id="content-main" class="clearfix">
<?php foreach (App::$mods as $modName => $modConf) {
    $modHome = $modConf['path'].DS.'home'.EXT;
    if ($modConf['enabled'] === true && file_exists($modHome)) include $modHome;
} ?>
</div>
<?php template('footer') ?>
