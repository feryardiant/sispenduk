$(function () {
    // Jquery UI Tab Trigger
    $('.jqui-tabs').hide()
    $(document).ready(function () {
        $('.jqui-tabs').show().tabs()
    })

    // Jquery UI Datepicker Trigger
    $('.jqui-datepicker').datepicker({dateFormat: 'dd-mm-yy'})

    // Validasi formulir
    // $('.form-ajax').validate({
    //     errorElement: 'span'
    // })

    // Form Cancel button function
    $('#cancel-btn').click(function () {
        window.location.href = $('#kembali-btn').attr('href')
    })

    // Data Table Delete button function
    $('.btn-hapus').click(function (e) {
        if (!confirm($(this).data('confirm-text'))) {
            e.preventDefault()
        }
    })

    // Pengiriman data formulir
    // $('.form-ajax').on('submit', function (e) {
    //     $('.btn').attr('disabled', 'disabled')

    //     $.post($(this).attr('action'), $(this).serialize(), function (response) {
    //         if (response.message !== undefined)
    //             setAlert(response.message)

    //         if (response.redirect !== undefined) {
    //             window.setTimeout(function () {
    //                 window.location.href = response.redirect
    //             }, 2000)
    //         }

    //         $('.btn').removeAttr('disabled')
    //     }, 'json')

    //     e.preventDefault()
    // })
})
